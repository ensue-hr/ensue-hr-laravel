<?php

namespace App\Exceptions;

use NicoSystem\Exceptions\NicoException;

class UserNotRegisteredException extends NicoException
{
    protected $code = 403;

    protected string $respCode = "user_not_registered";
}
