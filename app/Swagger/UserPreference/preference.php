<?php
/**
 * @OA\Get(
 *     path="/api/user-preferences",
 *     summary="Fetch list of system user-preferences",
 *     tags={"User Preference Settings"},
 *     security={
 *           {"api_key": {}}
 *       },
 * @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="Paginate page to fetch data",
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="sort_by",
 *         in="query",
 *         description="Filter sort by",
 * @OA\Schema(
 *             type="string",
 *             enum={"key"},
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="sort_order",
 *         in="query",
 *         description="Filter sort order",
 * @OA\Schema(
 *             type="string",
 *             enum={"asc","desc"}
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="keyword",
 *         in="query",
 *         description="search by key",
 * @OA\Schema(
 *             type="string",
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="category",
 *         in="query",
 *         description="filter by category",
 * @OA\Schema(
 *             type="string",
 *         ),
 *     ),
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/UserPreferenceListResponseModel")
 *     )
 *
 * )
 */

/**
 * @OA\Get(
 *     path="/api/user-preferences/{user_preference_id}",
 *     summary="Get detail of a user preference",
 *     tags={"User Preference Settings"},
 *     security={
 *         {"api_key": {}}
 *     },
 *
 * @OA\Parameter(
 *         name="user_preference_id",
 *         in="path",
 *         description="ID of a user preference",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/UserPreferenceResponseModel")
 *     )
 * )
 */

/**
 * @OA\Put(
 *     path="/api/user-preferences/{user_preference_id}",
 *     summary="Update a user preference",
 *     tags={"User Preference Settings"},
 *     security={
 *           {"api_key": {}}
 *       },
 *
 * @OA\Parameter(
 *         name="user_preference_id",
 *         in="path",
 *         description="ID of a user preference",
 *         required=true,
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *          ),
 *     ),
 *
 * @OA\RequestBody(
 *         description="Form data",
 *         required=true,
 * @OA\JsonContent(@OA\Property(property="value", type="string"),),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/UserPreferenceResponseModel"),
 *     )
 * )
 */
