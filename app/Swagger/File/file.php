<?php
/**
 * @OA\Post(
 *     path="/api/files/upload",
 *     summary="upload any type of file",
 *     tags={"Upload Files"},
 *     security={
 *         {"api_key": {}}
 *     },
 * @OA\RequestBody(
 * @OA\MediaType(
 *              mediaType="multipart/form-data",
 * @OA\Schema(
 * @OA\Property(
 *                      property="file",
 *                      type="file",
 *                      format="binary"
 *                  ),
 *              ),
 *          )
 *      ),
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/FileUploadResponseModel")
 *     )
 * )
 */


/**
 * @OA\Post(
 *     path="/api/files/upload/multiple",
 *     summary="upload multiple files",
 *     tags={"Upload Files"},
 *     security={
 *         {"api_key": {}}
 *     },
 * @OA\RequestBody(
 * @OA\MediaType(
 *              mediaType="multipart/form-data",
 * @OA\Schema(
 * @OA\Property(
 *                      property="files[]",
 *                      type="array",
 * @OA\Items(
 *                          type="file",
 *                          format="binary",
 *                      ),
 *                  ),
 *              ),
 *          )
 *      ),
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/FileUploadListResponseModel")
 *     )
 * )
 */
