<?php
/**
 * @OA\Schema(
 *  schema="AttendanceCommonModel",
 *      allOf={
 * @OA\Schema(
 * @OA\Property(property="id",           type="integer", default="1", readOnly=true),
 * @OA\Property(property="type",         type="string"),
 * @OA\Property(property="browser",         type="string"),
 * @OA\Property(property="device",         type="string"),
 * @OA\Property(property="location",         type="string"),
 * @OA\Property(property="ip",         type="string", default="255.255.255.255"),
 *     )}
 * )
 */


/**
 * @OA\Schema(
 *  schema="AttendanceModel",
 *      allOf={
 * @OA\Schema(
 *          ref="#/components/schemas/AttendanceCommonModel",
 *     ),
 * @OA\Schema(
 * @OA\Property(property="attend_at",     type="string", default="YYYY-MM-DD H:i:s"),
 * @OA\Property(property="reason",       type="string"),
 * @OA\Property(property="status",     type="integer", default=1),
 * @OA\Property(property="created_at", type="string"),
 * @OA\Property(property="updated_at", type="string"),
 *     )
 *  }
 * )
 */

/**
 * @OA\Schema(
 *  schema="AttendanceParentModel",
 *      allOf={
 * @OA\Schema(
 * @OA\Property(property="id",         type="integer", default="1", readOnly=true),
 * @OA\Property(property="attend_at",       type="string"),
 * @OA\Property(property="reason",      type="string"),
 * @OA\Property(property="browser",     type="string"),
 * @OA\Property(property="device",       type="string"),
 * @OA\Property(property="location", type="string"),
 * @OA\Property(property="ip", type="string"),
 *     )}
 * )
 */

/**
 * @OA\Schema(
 *  schema="AttendanceEmployeeModel",
 *      allOf={
 * @OA\Schema(
 * @OA\Property(property="id",         type="integer", default="1", readOnly=true),
 * @OA\Property(property="name",       type="string"),
 * @OA\Property(property="email",      type="string"),
 * @OA\Property(property="avatar",     type="string"),
 * @OA\Property(property="code",       type="string"),
 * @OA\Property(property="department", type="string"),
 *     )}
 * )
 */

/**
 * @OA\Schema(
 *  schema="AttendanceUpdateRequestModel",
 *      allOf={
 * @OA\Schema(
 * @OA\Property(property="attend_at",         type="string", default="YYYY-MM-DD H:i:s"),
 * @OA\Property(property="reason",         type="string"),
 * @OA\Property(property="employee_id",           type="integer", default="1"),
 *     )}
 * )
 */


/**
 * @OA\Schema(
 *  schema="AttendanceDetailResponseModel",
 *  allOf={
 * @OA\Schema   (ref="#/components/schemas/AttendanceModel"),
 * @OA\Schema   (
 * @OA\Property (property="parents", type="array", @OA\Items(ref="#/components/schemas/AttendanceEmployeeModel")),
 *      )
 *  }
 * )
 */

/**
 * @OA\Schema(
 *  schema="AttendanceDetailsResponseModel",
 *  allOf={
 *
 * @OA\Schema   (@OA\Property(property="data", type="array", @OA\Items(ref="#/components/schemas/AttendanceDetailResponseModel"))),
 * @OA\Schema(ref="#/components/schemas/ListDataBody"),
 *  }
 * )
 */
