<?php
/**
 * @OA\Post                                                       (
 *     path="/api/attendances",
 *     summary="Punch in attendance. type:check_in, check_out, break_out, break_in",
 *     tags={"Attendances"},
 *     security={
 *           {"api_key": {}}
 *       },
 * @OA\RequestBody(
 *         description="Form Data",
 *         required=true,
 * @OA\JsonContent(ref="#/components/schemas/AttendanceCommonModel"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/AttendanceModel"),
 *     ),
 *
 * )
 */

/**
 * @OA\Put                                                       (
 *     path="/api/attendances/{attendance_id}",
 *     summary="Update attendance by employee and manager. When updating attendence by manager: add employee_id and by employee: remove employee_id",
 *     tags={"Attendances"},
 *     security={
 *           {"api_key": {}}
 *       },
 * @OA\Parameter(
 *         name="attendance_id",
 *         in="path",
 *         description="ID of a attendance",
 *         required=true,
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *          ),
 *     ),
 *
 * @OA\RequestBody(
 *         description="Form Data",
 *         required=true,
 * @OA\JsonContent(ref="#/components/schemas/AttendanceUpdateRequestModel"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/AttendanceModel"),
 *     ),
 *
 * )
 */

/**
 * @OA\Get(
 *     path="/api/attendances/details",
 *     summary="Get detail of a attendances of any date of employees. NOTE: this API will be modified once design is completed",
 *     tags={"Attendances"},
 *     security={
 *         {"api_key": {}}
 *     },
 *
 * @OA\Parameter(
 *         name="employee_id",
 *         in="query",
 *         description="ID of a employee",
 *         required=false,
 * @OA\Schema(type="integer"),
 *     ),
 * @OA\Parameter(
 *         name="date",
 *         in="query",
 *         description="Date in YYYY-MM-DD format",
 *         required=true,
 * @OA\Schema(type="string"),
 *     ),
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/AttendanceDetailsResponseModel")
 *     )
 * )
 */

