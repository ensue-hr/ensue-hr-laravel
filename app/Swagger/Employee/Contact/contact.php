<?php

/**
 * @OA\Get(
 *     path="/api/employees/{employee_id}/contacts",
 *     summary="Fetch list of employee contact",
 *     tags={"Employee Contacts"},
 *     security={
 *           {"api_key": {}}
 *       },
 * @OA\Parameter(
 *         name="employee_id",
 *         in="path",
 *         description="ID of a employee",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 * @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="Paginate page to fetch data",
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="per_page",
 *         in="query",
 *         description="Paginate per page to fetch data",
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="sort_by",
 *         in="query",
 *         description="Filter sort by",
 * @OA\Schema(
 *             type="string",
 *             enum={"type","number","status","created_at"},
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="sort_order",
 *         in="query",
 *         description="Filter sort order",
 * @OA\Schema(
 *             type="string",
 *             enum={"asc","desc"}
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="keyword",
 *         in="query",
 *         description="search by number",
 * @OA\Schema(
 *             type="string",
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="type",
 *         in="query",
 *         description="Filter by status: 1=PERSONAL, 2=HOME, 3=OFFICE",
 * @OA\Schema(
 *             type="string",
 *             enum={"1","2","3"}
 *         ),
 *     ),
 * @OA\Parameter(
 *         name="status",
 *         in="query",
 *         description="Filter by status: 1=UNPUBLISHED, 2=PUBLISHED",
 * @OA\Schema(
 *             type="string",
 *             enum={"1","2"}
 *         ),
 *     ),
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/ContactListResponseModel")
 *     )
 *
 * )
 */

/**
 * @OA\Get(
 *     path="/api/employees/{employee_id}/contacts/{contact_id}",
 *     summary="Get detail of a contact",
 *     tags={"Employee Contacts"},
 *     security={
 *         {"api_key": {}}
 *     },
 * @OA\Parameter(
 *         name="employee_id",
 *         in="path",
 *         description="ID of a employee",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 *
 * @OA\Parameter(
 *         name="contact_id",
 *         in="path",
 *         description="ID of a contact",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/ContactResponseModel")
 *     )
 * )
 */

/**
 * @OA\Post                                                        (
 *     path="/api/employees/{employee_id}/contacts",
 *     summary="Create new contact",
 *     tags={"Employee Contacts"},
 *     security={
 *           {"api_key": {}}
 *       },
 * @OA\Parameter(
 *         name="employee_id",
 *         in="path",
 *         description="ID of a employee",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 * @OA\RequestBody(
 *         description="Form Data",
 *         required=true,
 * @OA\JsonContent(ref="#/components/schemas/ContactCommonModel"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/ContactResponseModel"),
 *     ),
 *
 * )
 */

/**
 * @OA\Put(
 *     path="/api/employees/{employee_id}/contacts/{contact_id}",
 *     summary="Update a contact",
 *     tags={"Employee Contacts"},
 *     security={
 *           {"api_key": {}}
 *       },
 * @OA\Parameter(
 *         name="employee_id",
 *         in="path",
 *         description="ID of a employee",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 * @OA\Parameter(
 *         name="contact_id",
 *         in="path",
 *         description="ID of a contact",
 *         required=true,
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *          ),
 *     ),
 *
 * @OA\RequestBody(
 *         description="Form data",
 *         required=true,
 * @OA\JsonContent(ref="#/components/schemas/ContactCommonModel"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/ContactResponseModel"),
 *     )
 * )
 */

/**
 * @OA\Delete(
 *     path="/api/employees/{employee_id}/contacts/{contact_id}",
 *     summary="Delete a contact",
 *     tags={"Employee Contacts"},
 *     security={
 *         {"api_key": {}}
 *     },
 * @OA\Parameter(
 *         name="employee_id",
 *         in="path",
 *         description="ID of a employee",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 * @OA\Parameter(
 *         name="contact_id",
 *         in="path",
 *         description="ID of a Contact",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(type="object")
 *     ),
 * )
 */

/**
 * @OA\Put(
 *     path="/api/employees/{employee_id}/contacts/{contact_id}/status",
 *     summary="Toggle the status of leave",
 *     tags={"Employee Contacts"},
 *     security={
 *           {"api_key": {}}
 *       },
 * @OA\Parameter(
 *         name="employee_id",
 *         in="path",
 *         description="ID of a employee",
 *         required=true,
 * @OA\Schema(type="integer"),
 *     ),
 * @OA\Parameter(
 *         name="contact_id",
 *         in="path",
 *         description="ID of a Contact",
 *         required=true,
 * @OA\Schema(
 *             type="integer",
 *             minimum=1,
 *          ),
 *     ),
 *
 * @OA\Response(
 *          response=200,
 *          description="successful",
 * @OA\JsonContent(ref="#/components/schemas/ContactResponseModel"),
 *     )
 * )
 */
