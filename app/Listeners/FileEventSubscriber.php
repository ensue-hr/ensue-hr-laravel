<?php


namespace App\Listeners;


use App\Events\Employee\Document\Created as EmployeeDocumentCreated;
use App\Events\Employee\Document\Updated as EmployeeDocumentUpdated;
use App\Events\Employee\History\Deactivated as EmployeeDeactivated;
use App\Events\Employee\History\Activated as EmployeeActivated;
use App\Events\User\Updated as UserUpdated;
use App\Events\Client\Created as ClientCreated;
use App\Events\Client\Updated as ClientUpdated;
use App\Events\Project\Created as ProjectCreated;
use App\Events\Project\Updated as ProjectUpdated;
use App\Events\Project\Document\Created as ProjectDocumentCreated;
use App\Events\Project\Document\Updated as ProjectDocumentUpdated;
use App\System\Foundation\FileUpload\Upload;
use Illuminate\Support\Str;

class FileEventSubscriber
{
    use Upload;

    /**
     * @param string $source
     * @param $model
     * @param string $field
     */
    public function fileMove(string $source, $model, string $field): void
    {
        if (Str::contains($source, '/temp/')) {
            $path = $this->moveFromTemp($source);
            $model->{$field} = $path;
            $model->save();
        }
    }

    /**
     * @param \App\Events\Employee\Document\Created $event
     */
    public function onEmployeeDocumentCreated(EmployeeDocumentCreated $event): void
    {
        $this->fileMove($event->document->url, $event->document, 'url');
        if ($event->document->thumbnail) {
            $this->fileMove($event->document->thumbnail, $event->document, 'thumbnail');
        }
    }

    /**
     * @param \App\Events\Employee\Document\Updated $event
     */
    public function onEmployeeDocumentUpdated(EmployeeDocumentUpdated $event): void
    {
        $this->fileMove($event->document->url, $event->document, 'url');
        if ($event->document->thumbnail) {
            $this->fileMove($event->document->thumbnail, $event->document, 'thumbnail');
        }
    }

    /**
     * @param \App\Events\Employee\History\Deactivated $event
     */
    public function onEmployeeDeactivated(EmployeeDeactivated $event): void
    {
        $document = $event->history->document;
        if ($document) {
            $this->fileMove($document->url, $document, 'url');
            if ($document->thumbnail) {
                $this->fileMove($document->thumbnail, $document, 'thumbnail');
            }
        }
    }

    public function onEmployeeActivated(EmployeeActivated $event): void
    {
        $document = $event->history->document;
        if ($document) {
            $this->fileMove($document->url, $document, 'url');
            if ($document->thumbnail) {
                $this->fileMove($document->thumbnail, $document, 'thumbnail');
            }
        }
    }

    /**
     * @param \App\Events\User\Updated $event
     */
    public function onUserUpdated(UserUpdated $event): void
    {
        $user = $event->user;
        if ($user->avatar) {
            $this->fileMove($user->avatar, $user, 'avatar');
        }
    }

    /**
     * @param \App\Events\Client\Created $event
     */
    public function onClientCreated(ClientCreated $event): void
    {
        $client = $event->client;
        if ($client->avatar) {
            $this->fileMove($client->avatar, $client, 'avatar');
        }
    }

    /**
     * @param \App\Events\Client\Updated $event
     */
    public function onClientUpdated(ClientUpdated $event): void
    {
        $client = $event->client;
        if ($client->avatar) {
            $this->fileMove($client->avatar, $client, 'avatar');
        }
    }

    /**
     * @param \App\Events\Project\Created $event
     */
    public function onProjectCreated(ProjectCreated $event): void
    {
        $project = $event->project;
        if ($project->avatar) {
            $this->fileMove($project->avatar, $project, 'avatar');
        }
    }

    /**
     * @param \App\Events\Project\Updated $event
     */
    public function onProjectUpdated(ProjectUpdated $event): void
    {
        $project = $event->project;
        if ($project->avatar) {
            $this->fileMove($project->avatar, $project, 'avatar');
        }
    }

    /**
     * @param \App\Events\Project\Document\Created $event
     */
    public function onProjectDocumentCreated(ProjectDocumentCreated $event): void
    {
        $this->fileMove($event->document->url, $event->document, 'url');
        if ($event->document->thumbnail) {
            $this->fileMove($event->document->thumbnail, $event->document, 'thumbnail');
        }
    }

    /**
     * @param \App\Events\Employee\Document\Updated $event
     */
    public function onProjectDocumentUpdated(EmployeeDocumentUpdated $event): void
    {
        $this->fileMove($event->document->url, $event->document, 'url');
        if ($event->document->thumbnail) {
            $this->fileMove($event->document->thumbnail, $event->document, 'thumbnail');
        }
    }

    /**
     * @param $events
     */
    public function subscribe($events): void
    {
        $events->listen(EmployeeDocumentCreated::class, [FileEventSubscriber::class, 'onEmployeeDocumentCreated']);
        $events->listen(EmployeeDocumentUpdated::class, [FileEventSubscriber::class, 'onEmployeeDocumentUpdated']);
        $events->listen(EmployeeDeactivated::class, [FileEventSubscriber::class, 'onEmployeeDeactivated']);
        $events->listen(EmployeeActivated::class, [FileEventSubscriber::class, 'onEmployeeActivated']);
        $events->listen(UserUpdated::class, [FileEventSubscriber::class, 'onUserUpdated']);
        $events->listen(ClientCreated::class, [FileEventSubscriber::class, 'onClientCreated']);
        $events->listen(ClientUpdated::class, [FileEventSubscriber::class, 'onClientUpdated']);
        $events->listen(ProjectCreated::class, [FileEventSubscriber::class, 'onProjectCreated']);
        $events->listen(ProjectUpdated::class, [FileEventSubscriber::class, 'onProjectUpdated']);
        $events->listen(ProjectDocumentCreated::class, [FileEventSubscriber::class, 'onProjectDocumentCreated']);
        $events->listen(ProjectDocumentUpdated::class, [FileEventSubscriber::class, 'onProjectDocumentUpdated']);
    }

}
