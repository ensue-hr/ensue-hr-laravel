<?php

namespace App\Modules\Clients\Events;

use App\Modules\Clients\Database\Models\Client;

class Deleting
{
    /**
     * @param \App\Modules\Clients\Database\Models\Client $client
     */
    public function __construct(public Client $client)
    {
    }
}
