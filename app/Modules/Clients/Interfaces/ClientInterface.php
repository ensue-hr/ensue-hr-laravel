<?php

namespace App\Modules\Clients\Interfaces;

use NicoSystem\Interfaces\BasicCrudInterface;

/**
 * Interface ClientInterface
 * @package App\Modules\Clients\Interfaces
 */
interface ClientInterface extends BasicCrudInterface
{

}