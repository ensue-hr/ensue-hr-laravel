<?php

namespace App\Modules\Clients\Listeners;

use App\Modules\Clients\Events\Deleting;
use Illuminate\Events\Dispatcher;
use NicoSystem\Exceptions\ResourceInUseException;

class ClientEventSubscriber
{

    /**
     * @param \App\Modules\Clients\Events\Deleting $event
     */
    public function onClientDeleting(Deleting $event): void
    {
        if ($event->client->projects()->count() > 0) {
            throw new ResourceInUseException(trans('responses.client.has_project'), 'err_client_has_project');
        }
    }

    /**
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe(Dispatcher $events): void
    {
        $events->listen(Deleting::class, [ClientEventSubscriber::class, 'onClientDeleting']);
    }
}
