<?php


namespace App\Modules\Employees\Providers;

use App\Modules\Employees\Listeners\EmployeeEventSubscriber;
use App\Modules\Employees\Listeners\HistoryEventSubscriber;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event handler mappings for the application.
     *
     * @var array
     */
    protected $listen = [];

    /**
     * The subscriber classes to register.
     *
     * @var array
     */
    protected $subscribe = [
        EmployeeEventSubscriber::class,
        HistoryEventSubscriber::class,
    ];

}
