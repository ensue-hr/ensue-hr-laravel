<?php


namespace App\Modules\Employees\Filters;


use NicoSystem\Filters\BaseFilter;

class ContactsFilter extends BaseFilter
{
    /**
     * @param string $keyword
     */
    public function keyword(string $keyword = ''): void
    {
        $this->title($keyword);
    }

    /**
     * @param string $title
     */
    public function title(string $title = ''): void
    {
        if ($title !== '') {
            $this->builder->where('number', 'like', "%{$title}%");
        }
    }

    /**
     * @param string $type
     */
    public function type(string $type = ''): void
    {
        if ($type != '' || $type != null) {
            $this->builder->where('type', $type);
        }
    }
}
