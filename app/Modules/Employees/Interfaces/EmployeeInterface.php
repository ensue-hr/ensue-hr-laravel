<?php

namespace App\Modules\Employees\Interfaces;

use NicoSystem\Interfaces\BasicCrudInterface;

/**
 * Interface EmployeeInterface
 *
 * @package App\Modules\Employees\Interfaces
 */
interface EmployeeInterface extends BasicCrudInterface
{

}
