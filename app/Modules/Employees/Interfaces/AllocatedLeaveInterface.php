<?php

namespace App\Modules\Employees\Interfaces;

use App\Modules\Employees\Database\Models\AllocatedLeave;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use NicoSystem\Interfaces\BasicCrudInterface;

/**
 * Interface LeaveInterface
 *
 * @package App\Modules\Leaves\Interfaces
 */
interface AllocatedLeaveInterface extends BasicCrudInterface
{

    /**
     * @param  array  $inputs
     * @param  string $employeeId
     * @return \App\Modules\Employees\Database\Models\AllocatedLeave
     */
    public function createLeave(array $inputs, string $employeeId): AllocatedLeave;

    /**
     * @param  array  $inputs
     * @param  string $employeeId
     * @param  string $leaveId
     * @return \App\Modules\Employees\Database\Models\AllocatedLeave
     */
    public function updateLeave(array $inputs, string $employeeId, string $leaveId): AllocatedLeave;

    /**
     * @param  array  $inputs
     * @param  string $employeeId
     * @return \Illuminate\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection
     */
    public function getLeaveList(array $inputs, string $employeeId): LengthAwarePaginator|Collection;

    /**
     * @param  string $employeeId
     * @param  string $leaveId
     * @return \App\Modules\Employees\Database\Models\AllocatedLeave
     */
    public function getLeaveById(string $employeeId, string $leaveId): AllocatedLeave;

    /**
     * @param  string $employeeId
     * @param  string $leaveId
     * @return bool
     */
    public function destroyLeave(string $employeeId, string $leaveId): bool;

    /**
     * @param  string $employeeId
     * @param  string $leaveId
     * @return \App\Modules\Employees\Database\Models\AllocatedLeave
     */
    public function toggleLeaveStatus(string $employeeId, string $leaveId): AllocatedLeave;
}
