<?php

namespace App\Modules\Employees\Controllers;

use App\Modules\Employees\Interfaces\EmployeeInterface;
use App\Modules\Employees\Interfaces\EmployeeViewInterface;
use App\Modules\Employees\Requests\EmployeeCreateRequest;
use App\Modules\Employees\Requests\EmployeeListRequest;
use App\Modules\Employees\Requests\EmployeeUpdateRequest;
use Illuminate\Http\JsonResponse;
use NicoSystem\Controllers\BaseController;
use NicoSystem\Foundation\Requests\NicoListRequest;
use NicoSystem\Requests\NicoRequest;

/**
 * Class EmployeesController
 *
 * @package App\Modules\Employees\Controllers
 */
class EmployeesController extends BaseController
{
    /**
     * EmployeesController constructor.
     *
     * @param EmployeeInterface     $repository
     * @param EmployeeViewInterface $viewRepository
     */
    public function __construct(
        private EmployeeInterface $repository,
        private EmployeeViewInterface $viewRepository
    ) {
    }

    /**
     * @param \App\Modules\Employees\Requests\EmployeeListRequest $request
     * @return JsonResponse
     */
    public function index(EmployeeListRequest $request): JsonResponse
    {
        return $this->responseOk($this->viewRepository->getList($request->all()));
    }

    /**
     * @param  EmployeeCreateRequest $request
     * @return JsonResponse
     */
    public function store(EmployeeCreateRequest $request): JsonResponse
    {
        return $this->responseOk($this->repository->create($request->all()));
    }

    /**
     * @param  EmployeeUpdateRequest $request
     * @param  string                $id
     * @return JsonResponse
     */
    public function update(EmployeeUpdateRequest $request, string $id): JsonResponse
    {
        return $this->responseOk($this->repository->update($id, $request->all()));
    }

    /**
     * @param  NicoRequest $request
     * @param  string      $id
     * @return JsonResponse
     */
    public function destroy(NicoRequest $request, string $id): JsonResponse
    {
        return $this->responseOk($this->repository->destroy($id));
    }

    /**
     * @param  NicoRequest $request
     * @param  string      $id
     * @return JsonResponse
     */
    public function show(NicoRequest $request, string $id): JsonResponse
    {
        return $this->responseOk($this->viewRepository->getById($id));
    }
}
