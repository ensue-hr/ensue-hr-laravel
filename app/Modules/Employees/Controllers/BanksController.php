<?php


namespace App\Modules\Employees\Controllers;


use App\Modules\Employees\Interfaces\BankInterface;
use App\Modules\Employees\Requests\BankCreateRequest;
use App\Modules\Employees\Requests\BankUpdateRequest;
use Illuminate\Http\JsonResponse;
use NicoSystem\Controllers\BaseController;
use NicoSystem\Foundation\Requests\NicoListRequest;
use NicoSystem\Requests\NicoRequest;

class BanksController extends BaseController
{

    /**
     * BanksController constructor.
     *
     * @param BankInterface $repository
     */
    public function __construct(private BankInterface $repository)
    {
    }

    /**
     * @param \NicoSystem\Foundation\Requests\NicoListRequest $request
     * @param string $employeeId
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(NicoListRequest $request, string $employeeId): JsonResponse
    {
        return $this->responseOk($this->repository->getBankList($request->all(), $employeeId));
    }

    /**
     * @param  \App\Modules\Employees\Requests\BankCreateRequest $request
     * @param  string                                            $employeeId
     * @return JsonResponse
     */
    public function store(BankCreateRequest $request, string $employeeId): JsonResponse
    {
        return $this->responseOk($this->repository->createBank($request->only('name', 'branch', 'account_number', 'status'), $employeeId));
    }

    /**
     * @param  \App\Modules\Employees\Requests\BankUpdateRequest $request
     * @param  string                                            $employeeId
     * @param  string                                            $bankId
     * @return JsonResponse
     */
    public function update(BankUpdateRequest $request, string $employeeId, string $bankId): JsonResponse
    {
        return $this->responseOk($this->repository->updateBank($request->only('name', 'branch', 'account_number', 'status'), $employeeId, $bankId));
    }

    /**
     * @param  NicoRequest $request
     * @param  string      $employeeId
     * @param  string      $bankId
     * @return JsonResponse
     */
    public function destroy(NicoRequest $request, string $employeeId, string $bankId): JsonResponse
    {
        return $this->responseOk($this->repository->destroyBank($employeeId, $bankId));
    }

    /**
     * @param  \NicoSystem\Requests\NicoRequest $request
     * @param  string                           $employeeId
     * @param  string                           $bankId
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(NicoRequest $request, string $employeeId, string $bankId): JsonResponse
    {
        return $this->responseOk($this->repository->getBankById($employeeId, $bankId));
    }

    /**
     * @param NicoRequest $request
     * @param string $employeeId
     * @param string $bankId
     * @return JsonResponse
     */
    public function toggleStatus(NicoRequest $request, string $employeeId, string $bankId): JsonResponse
    {
        return $this->responseOk($this->repository->toggleBankStatus($employeeId, $bankId));
    }
}
