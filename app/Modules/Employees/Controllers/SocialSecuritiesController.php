<?php


namespace App\Modules\Employees\Controllers;


use App\Modules\Employees\Interfaces\SocialSecurityInterface;
use App\Modules\Employees\Requests\SocialSecurityCreateRequest;
use App\Modules\Employees\Requests\SocialSecurityUpdateRequest;
use Illuminate\Http\JsonResponse;
use NicoSystem\Controllers\BaseController;
use NicoSystem\Foundation\Requests\NicoListRequest;
use NicoSystem\Requests\NicoRequest;

class SocialSecuritiesController extends BaseController
{
    /**
     * DepartmentsController constructor.
     *
     * @param \App\Modules\Employees\Interfaces\SocialSecurityInterface $repository
     */
    public function __construct(private SocialSecurityInterface $repository)
    {
    }

    /**
     * @param  \NicoSystem\Foundation\Requests\NicoListRequest $request
     * @param  string                                          $employeeId
     * @return JsonResponse
     */
    public function index(NicoListRequest $request, string $employeeId): JsonResponse
    {
        return $this->responseOk($this->repository->getSocialSecurityList($request->all(), $employeeId));
    }

    /**
     * @param  \App\Modules\Employees\Requests\SocialSecurityCreateRequest $request
     * @param  string                                                      $employeeId
     * @return JsonResponse
     */
    public function store(SocialSecurityCreateRequest $request, string $employeeId): JsonResponse
    {
        return $this->responseOk($this->repository->createSocialSecurity($request->except('end_date'), $employeeId));
    }

    /**
     * @param  \App\Modules\Employees\Requests\SocialSecurityUpdateRequest $request
     * @param  string                                                      $employeeId
     * @param  string                                                      $securityId
     * @return JsonResponse
     */
    public function update(SocialSecurityUpdateRequest $request, string $employeeId, string $securityId): JsonResponse
    {
        return $this->responseOk($this->repository->updateSocialSecurity($request->all(), $employeeId, $securityId));
    }

    /**
     * @param  NicoRequest $request
     * @param  string      $employeeId
     * @param  string      $securityId
     * @return JsonResponse
     */
    public function destroy(NicoRequest $request, string $employeeId, string $securityId): JsonResponse
    {
        return $this->responseOk($this->repository->destroySocialSecurity($employeeId, $securityId));
    }

    /**
     * @param  NicoRequest $request
     * @param  string      $employeeId
     * @param  string      $securityId
     * @return JsonResponse
     */
    public function show(NicoRequest $request, string $employeeId, string $securityId): JsonResponse
    {
        return $this->responseOk($this->repository->getSocialSecurityById($employeeId, $securityId));
    }

    /**
     * @param  NicoRequest $request
     * @param  string      $employeeId
     * @param  string      $securityId
     * @return JsonResponse
     */
    public function toggleStatus(NicoRequest $request, string $employeeId, string $securityId): JsonResponse
    {
        return $this->responseOk($this->repository->toggleSocialSecurityStatus($employeeId, $securityId));
    }
}
