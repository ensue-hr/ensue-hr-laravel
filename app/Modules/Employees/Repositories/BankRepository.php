<?php


namespace App\Modules\Employees\Repositories;


use App\Exceptions\ResourceExistsException;
use App\Modules\Employees\Database\Models\Bank;
use App\Modules\Employees\Database\Models\Employee;
use App\Modules\Employees\Filters\BankFilter;
use App\Modules\Employees\Interfaces\BankInterface;
use App\System\Common\Database\Models\Bank as SystemBank;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use NicoSystem\Repositories\BaseRepository;

class BankRepository extends BaseRepository implements BankInterface
{
    /**
     * @var \App\Modules\Employees\Database\Models\Employee
     */
    private Employee $globalEmployee;

    /**
     * BankRepository constructor.
     *
     * @param Employee $employee
     * @param \App\Modules\Employees\Database\Models\Bank $bank
     */
    public function __construct(private Employee $employee, Bank $bank)
    {
        parent::__construct($bank);
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $builder
     * @return \App\Modules\Employees\Filters\BankFilter
     */
    public function getFilter(Builder $builder): BankFilter
    {
        return new BankFilter($builder);
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $builder
     */
    public function onBeforeResult(Builder $builder): void
    {
        $builder->where(
            [
                'bankable_id' => $this->globalEmployee->id,
                'bankable_type' => $this->employee->getMorphClass(),
            ]
        );
    }

    /**
     * @param  array  $params
     * @param  string $employeeId
     * @return \Illuminate\Support\Collection|\Illuminate\Pagination\LengthAwarePaginator
     */
    public function getBankList(array $params, string $employeeId): Collection|LengthAwarePaginator
    {
        $this->globalEmployee = $this->employee->findOrfail($employeeId);

        return parent::getList($params);
    }

    /**
     * @param array $inputs
     * @param string $employeeId
     * @return \App\Modules\Employees\Database\Models\Bank
     */
    public function createBank(array $inputs, string $employeeId): Bank
    {
        $employee = $this->employee->findOrfail($employeeId);
        if ($employee->bank) {
            throw new ResourceExistsException(trans('responses.employee.bank.registered'), 'err_bank_exists_exception');
        }

        $inputs['bankable_id'] = $employeeId;
        $inputs['bankable_type'] = $this->employee->getMorphClass();

        return parent::create($inputs);
    }

    /**
     * @param  array  $inputs
     * @param  string $employeeId
     * @param  string $bankId
     * @return Bank
     */
    public function updateBank(array $inputs, string $employeeId, string $bankId): Bank
    {
        $this->getBankById($employeeId, $bankId);

        return parent::update($bankId, $inputs);
    }

    /**
     * @param  string $employeeId
     * @param  string $bankId
     * @return bool
     */
    public function destroyBank(string $employeeId, string $bankId): bool
    {
        $bank = $this->getBankById($employeeId, $bankId);

        return $bank->delete();
    }

    /**
     * @param  string $employeeId
     * @param  string $bankId
     * @return SystemBank
     */
    public function getBankById(string $employeeId, string $bankId): SystemBank
    {
        $employee = $this->employee->findOrfail($employeeId);

        return $employee->bank()->findOrfail($bankId);
    }

    /**
     * @param string $employeeId
     * @param string $bankId
     * @return \App\Modules\Employees\Database\Models\Bank
     */
    public function toggleBankStatus(string $employeeId, string $bankId): Bank
    {
        $this->getBankById($employeeId, $bankId);

        return parent::toggleStatus($bankId);
    }
}
