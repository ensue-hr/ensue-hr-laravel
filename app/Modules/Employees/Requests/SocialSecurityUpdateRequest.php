<?php


namespace App\Modules\Employees\Requests;


class SocialSecurityUpdateRequest extends SocialSecurityCreateRequest
{

    /**
     * @return array
     */
    public function rules(): array
    {
        $rules = parent::rules();
        $rules['end_date'] = 'nullable|date_format:Y-m-d|after:start_date|before_or_equal:now';

        return $rules;
    }
}
