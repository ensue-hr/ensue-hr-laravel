<?php
namespace App\Modules\Employees\Requests;

use App\System\Employee\Foundation\Gender;
use App\System\Employee\Foundation\MaritalStatus;
use Illuminate\Validation\Rule;
use NicoSystem\Foundation\Status;
use NicoSystem\Requests\NicoRequest;

/**
 * Class EmployeeUpdateRequest
 *
 * @package App\Modules\Employees\Requests
 */
class EmployeeUpdateRequest extends NicoRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        $employeeId = $this->route('employee');

        return [
            'first_name' => 'required|string|max:30',
            'last_name' => 'required|string|max:30',
            'middle_name' => 'nullable|string|max:30',
            'personal_email' => "required|email:rfc,dns|max:255|unique:users,email,NULL,id,deleted_at,NULL|unique:employees,personal_email,{$employeeId},id,deleted_at,NULL",
            'position' => 'required|string|max:50',
            'joined_at' => 'required|date_format:Y-m-d',
            'gender' => 'required|in:' . implode(',', Gender::options()),
            'dob' => 'required|date_format:Y-m-d|before:now',
            'marital_status' => 'required|in:' . implode(',', MaritalStatus::options()),
            'pan_no' => 'nullable|integer|digits:9',
            'department_id' => [
                'required',
                'integer',
                Rule::exists('departments', 'id')
                    ->whereNull('deleted_at')
                    ->where('status', Status::STATUS_PUBLISHED)
            ],
        ];
    }
}
