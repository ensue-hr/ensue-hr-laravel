<?php


namespace App\Modules\Employees\Requests;


use Illuminate\Validation\Rule;
use NicoSystem\Foundation\Status;
use NicoSystem\Requests\NicoRequest;

class BankCreateRequest extends NicoRequest
{

    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|max:100',
            'branch' => 'required|string|max:100',
            'account_number' => [
                'required',
                'alpha_dash',
                'min:9',
                'max:30',
                Rule::unique('banks', 'account_number')
                    ->where('bankable_type', 'Employee')
                    ->whereNull('deleted_at')
            ],
            'status' => 'required|in:' . implode(',', Status::options()),
        ];
    }

}
