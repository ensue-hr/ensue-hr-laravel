<?php

namespace App\Modules\UserPreferences\Controllers;

use App\Modules\UserPreferences\Interfaces\UserPreferenceInterface;
use App\Modules\UserPreferences\Requests\UserPreferenceUpdateRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use NicoSystem\Controllers\BaseController;
use NicoSystem\Foundation\Requests\NicoListRequest;
use NicoSystem\Requests\NicoRequest;

/**
 * Class UserPreferencesController
 * @package App\Modules\UserPreferences\Controllers
 */
class UserPreferencesController extends BaseController
{
    /**
     * UserPreferencesController constructor.
     * @param UserPreferenceInterface $repository
     */
    public function __construct(private UserPreferenceInterface $repository)
    {
    }

    /**
     * @param \NicoSystem\Foundation\Requests\NicoListRequest $request
     * @return JsonResponse
     */
    public function index(NicoListRequest $request): JsonResponse
    {
        $authUser = Auth::user();
        return $this->responseOk($this->repository->getUserPreferencesList($authUser->id, $request->all()));
    }

    /**
     * @param UserPreferenceUpdateRequest $request
     * @param string $id
     * @return JsonResponse
     */
    public function update(UserPreferenceUpdateRequest $request, string $id): JsonResponse
    {
        $authUser = Auth::user();
        return $this->responseOk($this->repository->updateUserPreference($id, $authUser->id, $request->only('value')));
    }

    /**
     * @param NicoRequest $request
     * @param string $id
     * @return JsonResponse
     */
    public function show (NicoRequest $request, string $id): JsonResponse
    {
        $authUser = Auth::user();
        return $this->responseOk($this->repository->getUserPreferenceById($id, $authUser->id));
    }
}
