<?php

namespace App\Modules\UserPreferences\Interfaces;

use App\Modules\UserPreferences\Database\Models\UserPreference;
use Illuminate\Pagination\LengthAwarePaginator;
use NicoSystem\Interfaces\BasicCrudInterface;

/**
 * Interface UserPreferenceInterface
 * @package App\Modules\UserPreferences\Interfaces
 */
interface UserPreferenceInterface extends BasicCrudInterface
{
    /**
     * @param int $userId
     * @param array $params
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function getUserPreferencesList(int $userId, array $params = []): LengthAwarePaginator;

    /**
     * @param string $preferenceId
     * @param int $userId
     * @param array $inputs
     * @return \App\Modules\UserPreferences\Database\Models\UserPreference
     */
    public function updateUserPreference(string $preferenceId, int $userId, array $inputs): UserPreference;

    /**
     * @param string $preferenceId
     * @param int $userId
     * @return \App\Modules\UserPreferences\Database\Models\UserPreference
     */
    public function getUserPreferenceById(string $preferenceId, int $userId): UserPreference;
}
