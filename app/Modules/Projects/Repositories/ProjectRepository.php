<?php

namespace App\Modules\Projects\Repositories;

use App\Events\Project\Updated;
use App\Events\Project\Created;
use App\Modules\Projects\Filters\ProjectsFilter;
use App\Modules\Projects\Interfaces\ProjectInterface;
use App\Modules\Projects\Database\Models\Project;
use App\System\Project\Foundation\ProjectStatus;
use NicoSystem\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ProjectRepository
 * @package App\Modules\Projects\Repositories
 */
class ProjectRepository extends BaseRepository implements ProjectInterface
{
    /**
     * @var array|string[]
     */
    protected array $events = [
        'created' => Created::class,
        'updated' => Updated::class,
    ];

    /**
     * ProjectRepository constructor.
     * @param Project $model
     */
    public function __construct(Project $model)
    {
        parent::__construct($model);
    }

    /**
     * @param Builder $builder
     * @return ProjectsFilter
     */
    public function getFilter(Builder $builder): ProjectsFilter
    {
        return new ProjectsFilter($builder);
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $builder
     */
    public function onBeforeResult(Builder $builder): void
    {
        $builder->select('id', 'title', 'code', 'type', 'avatar', 'status', 'start_date', 'end_date', 'created_at', 'updated_at', 'client_id')
        ->with('client');
    }

    /**
     * @param string $id
     * @param array $attributes
     * @return \App\Modules\Projects\Database\Models\Project
     */
    public function getById(string $id, array $attributes = []): Project
    {
        return $this->model->with(['client', 'documents']   )->findOrFail($id);
    }

    /**
     * @param array $inputs
     * @return \App\Modules\Projects\Database\Models\Project
     */
    public function create(array $inputs): Project
    {
        $inputs['status'] = ProjectStatus::STATUS_UNARCHIVE;
        return parent::create($inputs);
    }

    /**
     * @param string $projectId
     */
    public function archive(string $projectId): void
    {
        $project = $this->model->newQuery()
            ->where('status', ProjectStatus::STATUS_UNARCHIVE)
            ->findOrFail($projectId);

        $project->update(['status' => ProjectStatus::STATUS_ARCHIVE]);
    }

}
