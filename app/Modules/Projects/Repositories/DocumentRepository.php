<?php


namespace App\Modules\Projects\Repositories;


use App\Events\Project\Document\Created;
use App\Events\Project\Document\Updated;
use App\Modules\Projects\Database\Models\Document;
use App\Modules\Projects\Database\Models\Project;
use App\Modules\Projects\Filters\DocumentsFilter;
use App\Modules\Projects\Interfaces\DocumentInterface;
use App\System\Common\Database\Models\Document as SystemDocument;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use NicoSystem\Repositories\BaseRepository;

class DocumentRepository extends BaseRepository implements DocumentInterface
{
    /**
     * @var Project
     */
    private Project $globalProject;

    /**
     * @var array|string[]
     */
    protected array $events = [
        'created' => Created::class,
        'updated' => Updated::class,
    ];

    /**
     * DocumentRepository constructor.
     *
     * @param Project $project
     * @param Document $document
     */
    public function __construct(
        private Project $project,
        Document $document
    ) {
        parent::__construct($document);
    }

    /**
     * @param Builder $builder
     * @return DocumentsFilter
     */
    public function getFilter(Builder $builder): DocumentsFilter
    {
        return new DocumentsFilter($builder);
    }

    /**
     * @param Builder $builder
     */
    public function onBeforeResult(Builder $builder): void
    {
        $builder->where(
            [
            'documentary_id' => $this->globalProject->id,
            'documentary_type' => $this->project->getMorphClass(),
            ]
        );
    }

    /**
     * @param  array  $params
     * @param  string $projectId
     * @return Collection|LengthAwarePaginator
     */
    public function getDocumentList(array $params, string $projectId): Collection|LengthAwarePaginator
    {
        $this->globalProject = $this->project->findOrfail($projectId);

        return parent::getList($params);
    }

    /**
     * @param array $inputs
     * @param string $projectId
     * @return Document
     */
    public function createDocument(array $inputs, string $projectId): Document
    {
        $this->project->findOrfail($projectId);
        $inputs['documentary_id'] = $projectId;
        $inputs['documentary_type'] = $this->project->getMorphClass();

        return parent::create($inputs);
    }

    /**
     * @param array $inputs
     * @param string $projectId
     * @param string $documentId
     * @return Document
     */
    public function updateDocument(array $inputs, string $projectId, string $documentId): Document
    {
        $this->getDocumentById($projectId, $documentId);

        return parent::update($documentId, $inputs);
    }

    /**
     * @param  string $projectId
     * @param  string $documentId
     * @return boolean
     */
    public function destroyDocument(string $projectId, string $documentId): bool
    {
        $document = $this->getDocumentById($projectId, $documentId);

        return $document->delete();
    }

    /**
     * @param  string $projectId
     * @param  string $documentId
     * @return SystemDocument
     */
    public function getDocumentById(string $projectId, string $documentId): SystemDocument
    {
        $project = $this->project->findOrfail($projectId);

        return $project->documents()->findOrfail($documentId);
    }
}
