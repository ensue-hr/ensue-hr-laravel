<?php

namespace App\Modules\Projects\Requests;

use App\System\AppBaseModel;
use Illuminate\Validation\Rule;
use NicoSystem\Requests\NicoRequest;

class AssignMemberRequest extends NicoRequest
{
    /**
     * @return \string[][]
     */
    public function rules(): array
    {
        return [
            'employee_ids' => 'required|array|min:1',
            'employee_ids.*' => [
                'required',
                'integer',
                'distinct',
                Rule::exists('vw_employees', 'id')
                    ->where('status', AppBaseModel::STATUS_ACTIVE)
                    ->whereNull('deleted_at')
            ],
            'role_id' => [
                'required',
                'integer',
                Rule::exists('project_roles', 'id')
                    ->whereNull('deleted_at')
            ]
        ];
    }

}
