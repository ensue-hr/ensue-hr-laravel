<?php
namespace App\Modules\Projects\Requests;

use App\System\Setting\Database\Models\Setting;
use App\System\Setting\Foundation\SettingKey;
use Illuminate\Validation\Rule;
use NicoSystem\Foundation\Status;
use NicoSystem\Requests\NicoRequest;

/**
 * Class ProjectCreateRequest
 * @package App\Modules\Projects\Requests
 */
class ProjectCreateRequest extends NicoRequest
{

    public function rules(): array
    {
        $projectType = Setting::where('key', SettingKey::PROJECT_TYPE)->first();

        return [
            'title' => 'required|string|max:100',
            'description' => 'nullable|string|max:500',
            'code' => 'required|string|max:10|unique:projects,code,NULL,id,deleted_at,NULL',
            'type' => 'required|string|in:' . implode(',', $projectType?->value ?? []),
            'avatar' => 'nullable|url|ends_with:.jpg,.png,.jpeg|max:255|starts_with:' . config('fileupload.whitelist_urls'),
            'start_date' => 'required|date_format:Y-m-d',
            'client_id' =>  [
                'required',
                'integer',
                Rule::exists('clients', 'id')
                    ->whereNull('deleted_at')
            ],
        ];
    }

}
