<?php

namespace App\Modules\Projects\Controllers;

use App\Modules\Projects\Interfaces\RoleInterface;
use App\Modules\Projects\Requests\RoleCreateRequest;
use App\Modules\Projects\Requests\RoleUpdateRequest;
use Illuminate\Http\JsonResponse;
use NicoSystem\Controllers\BaseController;
use NicoSystem\Foundation\Requests\NicoListRequest;
use NicoSystem\Requests\NicoRequest;

class RolesController extends BaseController
{
    /**
     * ProjectsController constructor.
     * @param \App\Modules\Projects\Interfaces\RoleInterface $repository
     */
    public function __construct(private RoleInterface $repository)
    {
    }

    /**
     * @param \NicoSystem\Foundation\Requests\NicoListRequest $request
     * @return JsonResponse
     */
    public function index(NicoListRequest $request): JsonResponse
    {
        return $this->responseOk($this->repository->getList($request->all()));
    }

    /**
     * @param \App\Modules\Projects\Requests\RoleCreateRequest $request
     * @return JsonResponse
     */
    public function store(RoleCreateRequest $request): JsonResponse
    {
        return $this->responseOk($this->repository->create($request->only('title')));
    }

    /**
     * @param \App\Modules\Projects\Requests\RoleUpdateRequest $request
     * @param string $id
     * @return JsonResponse
     */
    public function update(RoleUpdateRequest $request, string $id): JsonResponse
    {
        return $this->responseOk($this->repository->update($id, $request->only('title')));
    }

    /**
     * @param  NicoRequest $request
     * @param  string      $id
     * @return JsonResponse
     */
    public function destroy(NicoRequest $request, string $id): JsonResponse
    {
        return $this->responseOk($this->repository->destroy($id));
    }
}
