<?php

namespace App\Modules\Projects\Controllers;

use App\Modules\Projects\Interfaces\MemberInterface;
use App\Modules\Projects\Requests\AssignMemberRequest;
use App\Modules\Projects\Requests\RevokeMemberRequest;
use Illuminate\Http\JsonResponse;
use NicoSystem\Controllers\BaseController;

class MembersController extends BaseController
{

    /**
     * ProjectsController constructor.
     * @param \App\Modules\Projects\Interfaces\MemberInterface $repository
     */
    public function __construct(private MemberInterface $repository)
    {
    }

    /**
     * @param \App\Modules\Projects\Requests\AssignMemberRequest $request
     * @param string $projectId
     * @return JsonResponse
     */
    public function assignMembers(AssignMemberRequest $request, string $projectId): JsonResponse
    {
        $inputs = $request->only('role_id', 'employee_ids');
        $inputs['licensed'] = true;
        return $this->responseOk($this->repository->assignMembers($projectId, $inputs));
    }

    /**
     * @param \App\Modules\Projects\Requests\RevokeMemberRequest $request
     * @param string $projectId
     * @return \Illuminate\Http\JsonResponse
     */
    public function revokeMember(RevokeMemberRequest $request, string $projectId): JsonResponse
    {
        return $this->responseOk($this->repository->revokeMember($projectId, $request->employee_id));
    }
}
