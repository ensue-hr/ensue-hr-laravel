<?php

namespace App\Modules\Projects\Mails;

use App\Modules\Projects\Database\Models\Project;
use App\System\Employee\Database\Models\EmployeeView;
use Illuminate\Mail\Mailable;

class ProjectRevokeMemberMail extends Mailable
{
    /**
     * EmployeeDeactivateMail constructor.
     *
     * @param \App\Modules\Projects\Database\Models\Project $project
     * @param \App\System\Employee\Database\Models\EmployeeView $employee
     */
    public function __construct(private Project $project, private EmployeeView $employee)
    {
    }


    /**
     * @return \App\Modules\Projects\Mails\ProjectRevokeMemberMail
     */
    public function build(): ProjectRevokeMemberMail
    {
        return $this->subject('Access revoked from the ' . $this->project->title)
            ->view(nico_view('emails.project_revoke_member', [], [], true))->with(
                [
                    'receiver' => $this->employee->first_name,
                    'title' => 'Access revoked from the ' . $this->project->title,
                ]
            );
    }
}
