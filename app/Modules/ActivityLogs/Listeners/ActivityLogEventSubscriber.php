<?php

namespace App\Modules\ActivityLogs\Listeners;

use App\Events\User\UserLoginLogout;
use App\Events\User\UserSocialiteLoggedIn;
use App\System\ActivityLog\ActivityLogger;
use Illuminate\Events\Dispatcher;

class ActivityLogEventSubscriber
{

    /**
     * @param \App\Events\User\UserLoginLogout $event
     */
    public function onUserLoginLogout(UserLoginLogout $event): void
    {
        $activityLogger = new ActivityLogger();
        $activityLogger->setTitle($event->message)->saveLog();
    }

    /**
     * @param \App\Events\User\UserSocialiteLoggedIn $event
     */
    public function onSocialiteLoggedIn(UserSocialiteLoggedIn $event): void
    {
        $activityLogger = new ActivityLogger();
        $activityLogger->setTitle($event->message)->saveLog($event->user->id);
    }

    /**
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe(Dispatcher $events): void
    {
        $events->listen(UserLoginLogout::class, [ActivityLogEventSubscriber::class, 'onUserLoginLogout']);
        $events->listen(UserSocialiteLoggedIn::class, [ActivityLogEventSubscriber::class, 'onSocialiteLoggedIn']);
    }

}
