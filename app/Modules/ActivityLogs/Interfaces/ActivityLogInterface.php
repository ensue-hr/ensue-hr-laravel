<?php

namespace App\Modules\ActivityLogs\Interfaces;

use NicoSystem\Interfaces\BasicCrudInterface;

/**
 * Interface ActivityLogInterface
 * @package App\Modules\ActivityLogs\Interfaces
 */
interface ActivityLogInterface extends BasicCrudInterface
{

}