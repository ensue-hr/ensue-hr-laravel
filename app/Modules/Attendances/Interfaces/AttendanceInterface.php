<?php

namespace App\Modules\Attendances\Interfaces;

use App\Modules\Attendances\Database\Models\Attendance;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use NicoSystem\Interfaces\BasicCrudInterface;

/**
 * Interface AttendanceInterface
 * @package App\Modules\Attendances\Interfaces
 */
interface AttendanceInterface extends BasicCrudInterface
{

    /**
     * @param \App\Modules\Attendances\Database\Models\Attendance $attendance
     * @param array $inputs
     * @return \App\Modules\Attendances\Database\Models\Attendance
     */
    public function updateAttendanceByEmployee(Attendance $attendance, array $inputs): Attendance;

    /**
     * @param \App\Modules\Attendances\Database\Models\Attendance $attendance
     * @param array $inputs
     * @return \App\Modules\Attendances\Database\Models\Attendance
     */
    public function updateAttendance(Attendance $attendance, array $inputs): Attendance;

    /**
     * @param string $id
     * @return \App\Modules\Attendances\Database\Models\Attendance
     */
    public function getAttendanceByIdExceptLeaves(string $id): Attendance;

    /**
     * @param array $inputs
     * @return \Illuminate\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection
     */
    public function getDetails(array $inputs): LengthAwarePaginator|Collection;
}
