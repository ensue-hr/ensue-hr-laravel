<?php

namespace App\Modules\Attendances\Exceptions;

use NicoSystem\Exceptions\NicoException;

class AttendanceUpdateDisableException extends NicoException
{
    /**
     * @var int
     */
    protected $code = 400;

    /**
     * @var string
     */
    protected string $respCode = 'err_attendance_update_disable';

}
