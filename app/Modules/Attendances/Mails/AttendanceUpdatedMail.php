<?php

namespace App\Modules\Attendances\Mails;

use App\Modules\Attendances\Database\Models\Attendance;
use Illuminate\Mail\Mailable;

class AttendanceUpdatedMail extends Mailable
{
    /**
     * EmployeeDeactivateMail constructor.
     *
     * @param \App\Modules\Attendances\Database\Models\Attendance $oldAttendance
     * @param \App\Modules\Attendances\Database\Models\Attendance $newAttendance
     */
    public function __construct(private Attendance $oldAttendance, private Attendance $newAttendance)
    {
    }

    /**
     * @return \App\Modules\Attendances\Mails\AttendanceUpdatedMail
     */
    public function build(): AttendanceUpdatedMail
    {
        $oldAttendance = $this->oldAttendance;
        $newAttendance = $this->newAttendance;
        $employee = $newAttendance->employee;

        return $this->subject($employee->first_name, ' updated attendance log')
            ->view(nico_view('emails.attendance_updated', [], [], true))->with(
                [
                    'title' => $employee->first_name, ' updated attendance log',
                    'employeeName' => $employee->name,
                    'oldAttendanceAttendAt' => $oldAttendance->attend_at,
                    'newAttendanceAttendAt' => $newAttendance->attend_at,
                    'reason' => $newAttendance->reason,
                    'type' => str_replace('_', ' ', $oldAttendance->type),
                ]
            );
    }
}
