<?php

namespace App\Modules\Attendances\Database\Models;

use App\System\Attendance\Database\Models\Attendance as BaseModel;
use App\System\Attendance\Foundation\AttendanceType;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class Attendance
 * @package App\Modules\Attendances\Database\Models
 */
class Attendance extends BaseModel
{

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeExceptLeaves(Builder $query): Builder
    {
        return $query->whereNotIn('type', [AttendanceType::LEAVE_IN, AttendanceType::LEAVE_OUT]);
    }
}
