<?php

namespace App\Modules\Attendances\Controllers;

use App\Modules\Attendances\Interfaces\AttendanceReportInterface;
use App\Modules\Attendances\Requests\AttendanceListRequest;
use App\Modules\Attendances\Requests\AttendanceReportRequest;
use App\Modules\Attendances\Requests\AttendanceTodayRequest;
use Illuminate\Http\JsonResponse;
use NicoSystem\Controllers\BaseController;
use NicoSystem\Requests\NicoRequest;

class AttendanceReportController extends BaseController
{
    /**
     * AttendancesController constructor.
     *
     * @param AttendanceReportInterface $attendanceViewRepository
     */
    public function __construct(private AttendanceReportInterface $attendanceViewRepository)
    {
    }

    /**
     * @param AttendanceListRequest $request
     *
     * @return JsonResponse
     */
    public function index(AttendanceListRequest $request): JsonResponse
    {
        return $this->responseOk($this->attendanceViewRepository->getList($request->all()));
    }

    /**
     * @param AttendanceReportRequest $request
     * @param string $employee
     *
     * @return JsonResponse
     */
    public function getEmployeeAttendanceSummary(AttendanceReportRequest $request, string $employee): JsonResponse
    {
        return $this->responseOk($this->attendanceViewRepository->getEmployeeAttendanceSummary($request->all(),
            $employee));
    }

    /**
     * @param AttendanceReportRequest $request
     * @param string $employee
     *
     * @return JsonResponse
     */
    public function getEmployeeAttendanceCalendar(AttendanceReportRequest $request, string $employee): JsonResponse
    {
        return $this->responseOk($this->attendanceViewRepository->getEmployeeAttendanceCalendar($request->all(),
            $employee));
    }

    /**
     * @param AttendanceReportRequest $request
     *
     * @return JsonResponse
     */
    public function getEmployeesAttendanceSummary(AttendanceReportRequest $request): JsonResponse
    {
        $result      = $this->attendanceViewRepository->getEmployeesReport($request->all(), 'employee_id');
        $workingDays = $this->attendanceViewRepository->getWorkingDays($request->all());

        return $this->responseOk($result->map(static function ($attendance) use ($workingDays) {
            $attendance->working_days = $workingDays;
            $attendance->absent_days  = $workingDays - $attendance->present_days - $attendance->leave_days;
            $attendance->absent_days  = ($attendance->absent_days > 0) ? $attendance->absent_days : 0;
            return $attendance;
        }));
    }

    /**
     * @param AttendanceReportRequest $request
     *
     * @return JsonResponse
     */
    public function getEmployeesAttendanceAverage(AttendanceReportRequest $request): JsonResponse
    {
        $result = $this->attendanceViewRepository->getEmployeesReport($request->all(), 'attend_date');

        return $this->responseOk($this->attendanceViewRepository->getEachDatesReport($request->all(), $result, [
            'present_days',
            'leave_days',
            'avg_check_in',
            'avg_check_out',
            'total_office_time',
            'avg_office_time',
            'total_work_time',
            'avg_work_time',
            'total_break_time',
            'avg_break_time'
        ]));
    }

    /**
     * @param AttendanceReportRequest $request
     *
     * @return JsonResponse
     */
    public function getEmployeesAttendanceCalendar(AttendanceReportRequest $request): JsonResponse
    {
        return $this->responseOk($this->attendanceViewRepository->getEmployeesAttendanceCalendar($request->all()));
    }

    /**
     * @param NicoRequest $request
     *
     * @return JsonResponse
     */
    public function getTodaySummary(NicoRequest $request): JsonResponse
    {
        return $this->responseOk($this->attendanceViewRepository->getTodaySummary());
    }

    /**
     * @param AttendanceTodayRequest $request
     *
     * @return JsonResponse
     */
    public function getTodayAttendance(AttendanceTodayRequest $request): JsonResponse
    {
        return $this->responseOk($this->attendanceViewRepository->getTodayAttendance($request->only('keyword')));
    }
}
