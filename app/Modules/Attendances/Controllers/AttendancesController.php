<?php

namespace App\Modules\Attendances\Controllers;

use App\Exceptions\ForbiddenException;
use App\Modules\Attendances\Interfaces\AttendanceInterface;
use App\Modules\Attendances\Requests\AttendanceCreateRequest;
use App\Modules\Attendances\Requests\AttendanceDetailRequest;
use App\Modules\Attendances\Requests\AttendanceUpdateRequest;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use NicoSystem\Controllers\BaseController;
use NicoSystem\Exceptions\NicoBadRequestException;

/**
 * Class AttendancesController
 * @package App\Modules\Attendances\Controllers
 */
class AttendancesController extends BaseController
{
    /**
     * AttendancesController constructor.
     * @param AttendanceInterface $repository
     */
    public function __construct(private AttendanceInterface $repository)
    {
    }

    /**
     * @param \App\Modules\Attendances\Requests\AttendanceDetailRequest $request
     * @return JsonResponse
     */
    public function getDetails(AttendanceDetailRequest $request): JsonResponse
    {
        return $this->responseOk($this->repository->getDetails($request->all()));
    }

    /**
     * @param AttendanceCreateRequest $request
     * @return JsonResponse
     */
    public function store(AttendanceCreateRequest $request): JsonResponse
    {
        $employee = Auth::user()->employee;
        if (!$employee) {
            throw new ForbiddenException(trans('auth.forbidden'));
        }
        $inputs = $request->only('type', 'browser', 'device', 'location', 'fingerprint');
        $inputs['ip'] = $request->ip();
        $inputs['attend_at'] = Carbon::now();
        $inputs['employee_id'] = $employee->id;

        return $this->responseOk($this->repository->create($inputs));
    }

    /**
     * @param AttendanceUpdateRequest $request
     * @param string $id
     * @return JsonResponse
     */
    public function update(AttendanceUpdateRequest $request, string $id): JsonResponse
    {
        $attendance = $this->repository->getAttendanceByIdExceptLeaves($id);
        $inputs = $request->only('reason', 'attend_at', 'browser', 'device', 'location', 'fingerprint');
        $inputs['ip'] = $request->ip();
        if ($request->has('employee_id')) {
            if ($attendance->employee_id != $request->get('employee_id')) {
                throw new NicoBadRequestException(trans('responses.attendance.invalid_employee'));
            }
            return $this->responseOk($this->repository->updateAttendance($attendance, $inputs));
        }
        if ($attendance->employee_id != Auth::user()->employee?->id) {
            throw new ForbiddenException(trans('auth.forbidden'));
        }
        return $this->responseOk($this->repository->updateAttendanceByEmployee($attendance, $inputs));
    }
}
