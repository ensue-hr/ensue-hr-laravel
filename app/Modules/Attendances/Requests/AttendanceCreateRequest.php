<?php
namespace App\Modules\Attendances\Requests;

use App\System\Attendance\Foundation\AttendanceType;
use NicoSystem\Requests\NicoRequest;

/**
 * Class AttendanceCreateRequest
 * @package App\Modules\Attendances\Requests
 */
class AttendanceCreateRequest extends NicoRequest
{

    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'type' => 'required|string|in:' . implode(',', AttendanceType::options()),
            'browser' => 'nullable|string|max:50',
            'device' => 'nullable|string|max:100',
            'location' => 'nullable|string|max:100',
            'ip' => 'nullable|ip',
            'fingerprint' => 'nullable|integer',
        ];
    }

}
