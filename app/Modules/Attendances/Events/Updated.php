<?php

namespace App\Modules\Attendances\Events;

use App\Modules\Attendances\Database\Models\Attendance;

class Updated
{

    /**
     * @param \App\Modules\Attendances\Database\Models\Attendance $oldAttendance
     * @param \App\Modules\Attendances\Database\Models\Attendance $newAttendance
     */
    public function __construct(public Attendance $oldAttendance, public Attendance $newAttendance)
    {
    }

}
