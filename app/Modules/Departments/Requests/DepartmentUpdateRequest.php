<?php
namespace App\Modules\Departments\Requests;

use NicoSystem\Foundation\Status;
use NicoSystem\Requests\NicoRequest;

/**
 * Class DepartmentUpdateRequest
 *
 * @package App\Modules\Departments\Requests
 */
class DepartmentUpdateRequest extends NicoRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        $departmentId = $this->route('department');

        return [
            'title' => "required|string|max:50|unique:departments,title,{$departmentId},id,deleted_at,NULL",
            'description' => 'required|string|max:500',
            'status' => 'required|in:' . implode(',', Status::options()),
        ];
    }
}
