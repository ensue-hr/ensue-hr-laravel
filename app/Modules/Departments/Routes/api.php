<?php

use App\Modules\Departments\Controllers\DepartmentsController;
use Illuminate\Support\Facades\Route;

Route::group(
    ['middleware' => ['authApi', 'role:manager']], function () {
        Route::put('departments/{department}/status', [DepartmentsController::class, 'toggleStatus']);
        Route::put('departments/{department}/hod', [DepartmentsController::class, 'updateHod']);
        Route::resource('departments', 'DepartmentsController')->except('create', 'edit');
    }
);
