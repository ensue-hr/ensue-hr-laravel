<?php
namespace App\Modules\Leaves;

use App\Modules\Leaves\Providers\EventServiceProvider;
use \Illuminate\Support\ServiceProvider as BaseServiceProvider;

/**
 * Class ServiceProvider
 *
 * @package App\Modules\Leaves
 */
class ServiceProvider extends BaseServiceProvider
{
    /**
     * @type bool
     */
    protected $defer = true;

    /**
     * Register your binding
     */
    public function register()
    {
        $this->app->bind('App\Modules\Leaves\Interfaces\LeaveInterface', 'App\Modules\Leaves\Repositories\LeaveRepository');
        $this->app->register(EventServiceProvider::class);
    }
}
