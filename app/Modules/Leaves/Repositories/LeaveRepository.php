<?php

namespace App\Modules\Leaves\Repositories;

use App\Events\Leave\ApproveLeaveRequest;
use App\Modules\Leaves\Database\Models\AllocatedLeave;
use App\Modules\Leaves\Database\Models\Employee;
use App\Modules\Leaves\Database\Models\Leave;
use App\Modules\Leaves\Database\Models\Setting;
use App\Modules\Leaves\Events\CancelLeaveRequest;
use App\Modules\Leaves\Events\Created;
use App\Modules\Leaves\Events\Updated;
use App\Modules\Leaves\Exceptions\AllocatedLeaveNotFoundException;
use App\Modules\Leaves\Exceptions\InvalidLeaveStatusException;
use App\Modules\Leaves\Exceptions\PaidLeaveExceedException;
use App\Modules\Leaves\Filters\LeavesFilter;
use App\Modules\Leaves\Interfaces\LeaveInterface;
use App\System\Leave\Foundation\LeaveStatus;
use App\System\Leave\Foundation\LeaveType;
use App\System\Setting\Database\Traits\FiscalYearTrait;
use App\System\Setting\Foundation\SettingKey;
use App\System\User\Database\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use NicoSystem\Exceptions\NicoBadRequestException;
use NicoSystem\Exceptions\NicoException;
use NicoSystem\Repositories\BaseRepository;

/**
 * Class LeaveRepository
 *
 * @package App\Modules\Leaves\Repositories
 */
class LeaveRepository extends BaseRepository implements LeaveInterface
{
    use FiscalYearTrait;

    /**
     * @var array|string[]
     */
    protected array $events = [
        'created' => Created::class,
        'updated' => Updated::class,
    ];

    /**
     * LeaveRepository constructor.
     *
     * @param Leave $model
     * @param AllocatedLeave $allocatedLeave
     * @param Setting $setting
     * @param Employee $employee
     */
    public function __construct(
        Leave                  $model,
        private AllocatedLeave $allocatedLeave,
        private Setting        $setting,
        private Employee       $employee
    )
    {
        parent::__construct($model);
    }

    /**
     * @param Builder $builder
     *
     * @return LeavesFilter
     */
    public function getFilter(Builder $builder): LeavesFilter
    {
        return new LeavesFilter($builder);
    }

    /**
     * @param Builder $builder
     */
    public function onBeforeResult(Builder $builder): void
    {
        $builder->with(
            [
                'requested' => function ($query) {
                    $query->select('id', 'name', 'email', 'avatar', 'code', 'department');
                },
                'employee' => function ($query) {
                    $query->select('id', 'name', 'email', 'avatar', 'code', 'department');
                },
            ]
        );
    }

    /**
     * @param string $id
     * @param array $attributes
     *
     * @return Leave
     */
    public function getById(string $id, array $attributes = []): Leave
    {
        return $this->model->with(
            [
                'requested' => function ($query) {
                    $query->select('id', 'name', 'email', 'avatar', 'code', 'department');
                },
                'employee' => function ($query) {
                    $query->select('id', 'name', 'email', 'avatar', 'code', 'department');
                },
            ]
        )->findOrFail($id);
    }

    /**
     * @param array $inputs
     * @param string $fiscalDateStart
     * @param string $fiscalDateEnd
     * @param string|null $leaveId
     *
     * @return array
     */
    public function saveSplitLeaveRequest(
        array  $inputs,
        string $fiscalDateStart,
        string $fiscalDateEnd,
        string $leaveId = null
    ): array
    {
        DB::beginTransaction();
        try {
            $endAt = $inputs['end_at'];
            $inputs['end_at'] = $fiscalDateEnd . ' 23:59:59';
            $leaves[0] = $this->saveLeaveRequest($inputs, $fiscalDateStart, $fiscalDateEnd, $leaveId);

            $fiscalDateStart = Carbon::parse($fiscalDateEnd)->addDay()->toDateString();
            $fiscalDateEnd = Carbon::parse($fiscalDateStart)->addYear()->subDay()->toDateString();
            $inputs['start_at'] = $fiscalDateStart . ' 00:00:00';
            $inputs['end_at'] = $endAt;
            $leaves[1] = $this->saveLeaveRequest($inputs, $fiscalDateStart, $fiscalDateEnd);
            DB::commit();
            return $leaves;
        } catch (AllocatedLeaveNotFoundException $exception) {
            DB::rollBack();
            throw new AllocatedLeaveNotFoundException($exception->getMessage());
        } catch (PaidLeaveExceedException $exception) {
            DB::rollBack();
            throw new PaidLeaveExceedException($exception->getMessage());
        } catch (Exception $e) {
            DB::rollBack();
            throw new NicoException($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param array $inputs
     * @param string $fiscalDateStart
     * @param string $fiscalDateEnd
     * @param string|null $leaveId
     *
     * @return Leave
     */
    public function saveLeaveRequest(
        array  $inputs,
        string $fiscalDateStart,
        string $fiscalDateEnd,
        string $leaveId = null
    ): Leave
    {
        $inputs['days'] = convert_date_time_range_to_days($inputs['start_at'], $inputs['end_at']);
        if ($inputs['type'] == LeaveType::UNPAID_LEAVE) {
            return parent::save($inputs, $leaveId);
        }
        $this->verifyRequestedLeave($inputs, $fiscalDateStart, $fiscalDateEnd, $leaveId);

        return parent::save($inputs, $leaveId);
    }

    /**
     * @param array $inputs
     * @param string $fiscalDateStart
     * @param string $fiscalDateEnd
     * @param int|null $exceptLeaveId
     *
     * @return bool
     */
    private function verifyRequestedLeave(
        array  $inputs,
        string $fiscalDateStart,
        string $fiscalDateEnd,
        int    $exceptLeaveId = null
    ): bool
    {
        $leaves = $this->model->whereDateBetween($fiscalDateStart, $fiscalDateEnd)
            ->where('employee_id', $inputs['employee_id'])
            ->whereNotIn('type', [LeaveType::CANCELLATION_LEAVE, LeaveType::UNPAID_LEAVE])
            ->where('id', '!=', $exceptLeaveId)
            ->get();
        $allocatedLeaves = $this->allocatedLeave
            ->where('employee_id', $inputs['employee_id'])
            ->publishedBetweenStartAndEndDate($fiscalDateStart, $fiscalDateEnd)
            ->get();

        if ($allocatedLeaves->isEmpty()) {
            throw new AllocatedLeaveNotFoundException(trans('responses.leave.empty_allocated_leave'));
        }

        $totalAllocatedLeaveDays = $allocatedLeaves->pluck('days')->sum();
        $leavesTakenDays = $leaves->pluck('days')->sum();

        $remainingLeaveDays = $totalAllocatedLeaveDays - $leavesTakenDays;
        $allowedLeaveDays = $remainingLeaveDays - $inputs['days'];
        if ($allowedLeaveDays < 0) {
            throw new PaidLeaveExceedException(trans('responses.leave.paid_leave_exceed'));
        }

        return true;
    }

    /**
     * @param array $inputs
     * @param string|null $leaveId
     *
     * @return Leave
     */
    public function saveCancellationLeave(array $inputs, string $leaveId = null): Leave
    {
        $this->model->whereExistsInBetween($inputs['start_at'], $inputs['end_at'])
            ->where('employee_id', $inputs['employee_id'])
            ->where('type', '!=', LeaveType::CANCELLATION_LEAVE)
            ->where('status', LeaveStatus::APPROVED)
            ->firstOrFail();
        $inputs['days'] = convert_date_time_range_to_days($inputs['start_at'], $inputs['end_at']);

        return parent::save($inputs, $leaveId);
    }

    /**
     * @param array $inputs
     * @param int|null $exceptLeaveId
     *
     * @return Leave|null
     */
    public function getLeaveLiesBetweenStartAndEndDate(array $inputs, int $exceptLeaveId = null): Leave|null
    {
        $builder = $this->model
            ->where('employee_id', $inputs['employee_id'])
            ->whereDateLiesIn($inputs['start_at'], $inputs['end_at'])
            ->whereNotIn('status', [LeaveStatus::DECLINED, LeaveStatus::CANCELLED]);
        if ($inputs['type'] == LeaveType::CANCELLATION_LEAVE) {
            $builder = $builder->where('type', LeaveType::CANCELLATION_LEAVE);
        }

        return $builder->where('id', '!=', $exceptLeaveId)->first();
    }

    /**
     * @param array $inputs
     * @param string $leaveId
     * @param User $authUser
     *
     * @return Leave
     */
    public function cancelLeaveRequest(array $inputs, string $leaveId, User $authUser): Leave
    {
        $leave = parent::getById($leaveId);
        $isAuthUser = $authUser->employee?->id == $leave->employee_id;
        $this->verifyLeaveUpdateRequest($isAuthUser, $leave);

        $leave->reason = $inputs['reason'];
        $leave->status = LeaveStatus::CANCELLED;
        $leave->save();
        event(new CancelLeaveRequest($leave, $authUser));

        return $leave;
    }

    /**
     * @param int $isAuthUser
     * @param Leave $leave
     *
     * @return bool
     */
    private function verifyLeaveUpdateRequest(int $isAuthUser, Leave $leave, bool $addDays = False): bool
    {
        if (!$isAuthUser) {
            throw new NicoBadRequestException(trans('responses.leave.cannot_change_status'),
                'err_cannot_change_leave_status');
        }
        if ($leave->status != LeaveStatus::PENDING) {
            throw new InvalidLeaveStatusException(trans('responses.leave.status_not_pending'));
        }
        $date = Carbon::now();
        if($addDays)
        {
            $date = $date->subDays(2);
        }
        if ($leave->is_emergency == false && $leave->start_at < $date) {
            throw new NicoBadRequestException(trans('responses.leave.leave_expired'), 'err_leave_expired_exception');
        }

        return true;
    }

    /**
     * @param array $inputs
     * @param string $leaveId
     * @param User $authUser
     *
     * @return Leave
     */
    public function declineLeaveRequest(array $inputs, string $leaveId, User $authUser): Leave
    {
        $leave = parent::getById($leaveId);
        $isAuthUser = $authUser->employee?->id == $leave->requested_to;
        $this->verifyLeaveUpdateRequest($isAuthUser, $leave,True);

        $leave->reason = $inputs['reason'];
        $leave->status = LeaveStatus::DECLINED;
        $leave->save();
        event(new CancelLeaveRequest($leave, $authUser));

        return $leave;
    }

    /**
     * @param string $leaveId
     * @param User $authUser
     *
     * @return Leave
     */
    public function approveLeaveRequest(string $leaveId, User $authUser): Leave
    {
        $leave = parent::getById($leaveId);
        $authEmployee = $authUser->employee;
        $this->verifyLeaveUpdateRequest((int)$authEmployee?->id == $leave->requested_to, $leave,True);

        $leave->status = LeaveStatus::APPROVED;
        $leave->save();
        event(new ApproveLeaveRequest($leave));

        return $leave;
    }

    /**
     * @param array $inputs
     * @param string $leaveId
     *
     * @return Leave
     */
    public function requestCancellationLeave(array $inputs, string $leaveId): Leave
    {
        $leave = $this->model
            ->where('employee_id', $inputs['employee_id'])
            ->where('type', '!=', LeaveType::CANCELLATION_LEAVE)
            ->where('status', LeaveStatus::APPROVED)
            ->findOrfail($leaveId);

        if (!($leave->start_at <= $inputs['start_at'] && $leave->end_at >= $inputs['end_at'])) {
            throw new NicoBadRequestException();
        }
        $inputs += [
            'requested_to' => $leave->requested_to,
            'status' => LeaveStatus::PENDING,
            'is_emergency' => false,
            'days' => convert_date_time_range_to_days($inputs['start_at'], $inputs['end_at']),
            'parent_id' => $leaveId,
            'parent_type' => $leave->type,
        ];

        return parent::create($inputs);
    }

    /**
     * @param string $employeeId
     *
     * @return array
     */
    public function getSummary(string $employeeId): array
    {
        $employee = $this->employee->findOrfail($employeeId);
        $allocatedLeave = $employee->allocatedLeaves()->currentFiscalYear()->published()->get();
        $leaves = $employee->leaves()
            ->where('status', LeaveStatus::APPROVED)
            ->currentFiscalYear('start_at', 'end_at')
            ->get();
        $cancellationLeaves = $leaves->where('type', LeaveType::CANCELLATION_LEAVE);
        $leaveTypes = $this->setting->where('key', SettingKey::LEAVES_TYPE)->first()?->value ?? [];

        $summary = array_reduce($leaveTypes,
            static function ($assoc, $value) use ($allocatedLeave, $leaves, $cancellationLeaves) {
                $assoc[$value] = [
                    'allocated' => $allocatedLeave->where('type', $value)->sum('days'),
                    'leaves' => $leaves->where('type',
                            $value)->sum('days') - $cancellationLeaves->where('parent_type', $value)->sum('days'),
                ];
                return $assoc;
            });

        return array_merge([
            'total' => [
                'allocated' => $allocatedLeave->sum('days'),
                'leaves' => $leaves->sum('days') - (2 * $cancellationLeaves->sum('days')),
            ]
        ], $summary ?? []);
    }
}
