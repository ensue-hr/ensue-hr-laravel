<?php
namespace App\Modules\Leaves\Requests;

use App\System\Leave\Foundation\LeaveType;

/**
 * Class LeaveUpdateRequest
 *
 * @package App\Modules\Leaves\Requests
 */
class LeaveUpdateRequest extends LeaveCreateRequest
{

    /**
     * @return array
     */
    public function rules(): array
    {
        $rules = parent::rules();
        $rules['type'] .= ',' . LeaveType::CANCELLATION_LEAVE;
        unset($rules['requested_to']);

        return $rules;
    }
}
