<?php

namespace App\Modules\Leaves\Requests;

use NicoSystem\Requests\NicoRequest;

class LeaveCancellationRequest extends NicoRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'title' => 'required|string|max:100',
            'description' => 'required|string',
            'start_at' => 'required|date_format:Y-m-d H:i:s|after_or_equal:today',
            'end_at' => 'required|date_format:Y-m-d H:i:s|after:start_at',
        ];
    }
}
