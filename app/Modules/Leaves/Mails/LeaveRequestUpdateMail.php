<?php

namespace App\Modules\Leaves\Mails;

use App\Modules\Leaves\Database\Models\Leave;
use Illuminate\Mail\Mailable;

class LeaveRequestUpdateMail extends Mailable
{
    /**
     * EmployeeDeactivateMail constructor.
     *
     * @param Leave $leave
     */
    public function __construct(private Leave $leave)
    {
    }

    /**
     * @return LeaveRequestUpdateMail
     */
    public function build(): LeaveRequestUpdateMail
    {
        $employeeView = $this->leave->employee;
        $requestedView = $this->leave->requested;
        return $this->subject('Updated: ' . $this->leave->title)
            ->view(nico_view('emails.leave_request_update', [], [], true))->with(
                [
                    'receiver' => $requestedView->first_name,
                    'sender' => $employeeView->name,
                    'title' => $this->leave->title,
                    'description' => $this->leave->description,
                    'start_at' => $this->leave->start_at,
                    'end_at' => $this->leave->end_at,
                    'days' => $this->leave->days,
                    'type' => $this->leave->type,
                    'is_emergency' => $this->leave->is_emergency,
                ]
            );
    }
}
