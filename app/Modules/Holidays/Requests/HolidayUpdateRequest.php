<?php
namespace App\Modules\Holidays\Requests;

/**
 * Class HolidayUpdateRequest
 * @package App\Modules\Holidays\Requests
 */
class HolidayUpdateRequest extends HolidayCreateRequest
{

    /**
     * @return array
     */
    public function rules(): array
    {
        $holidayId = $this->route('holiday');

        $rules = parent::rules();
        $rules['date'] = "required|date_format:Y-m-d|after:today|unique:holidays,date,{$holidayId},id,deleted_at,NULL";

        return $rules;
    }
}
