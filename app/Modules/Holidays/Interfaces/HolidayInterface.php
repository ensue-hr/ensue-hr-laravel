<?php

namespace App\Modules\Holidays\Interfaces;

use Illuminate\Support\Collection;
use NicoSystem\Interfaces\BasicCrudInterface;

/**
 * Interface HolidayInterface
 * @package App\Modules\Holidays\Interfaces
 */
interface HolidayInterface extends BasicCrudInterface
{

    public function createMultiple(array $inputs): Collection;

}
