<?php

namespace App\Modules\Holidays\Repositories;

use App\Modules\Holidays\Filters\HolidaysFilter;
use App\Modules\Holidays\Interfaces\HolidayInterface;
use App\Modules\Holidays\Database\Models\Holiday;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use NicoSystem\Exceptions\NicoBadRequestException;
use NicoSystem\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class HolidayRepository
 * @package App\Modules\Holidays\Repositories
 */
class HolidayRepository extends BaseRepository implements HolidayInterface
{
    /**
     * HolidayRepository constructor.
     * @param Holiday $model
     */
    public function __construct(Holiday $model)
    {
        parent::__construct($model);
    }

    /**
     * @param Builder $builder
     * @return HolidaysFilter
     */
    public function getFilter(Builder $builder): HolidaysFilter
    {
        return new HolidaysFilter($builder);
    }

    /**
     * @param array $inputs
     * @return \Illuminate\Support\Collection
     */
    public function createMultiple(array $inputs): Collection
    {
        $holidays = collect($inputs['holidays']);

        return $holidays->map(function ($item) {
            return parent::create($item);
        });
    }

    /**
     * @param string $id
     * @param array $attributes
     * @return \App\Modules\Holidays\Database\Models\Holiday
     */
    public function update(string $id, array $attributes): Holiday
    {
        $canUpdate = $this->canUpdateHoliday($id);
        if (!$canUpdate) {
            throw new NicoBadRequestException(trans('responses.holiday.passed'), 'err_holiday_passed');
        }
        return parent::update($id, $attributes);
    }

    /**
     * @param string $id
     * @param array $options
     * @return bool
     */
    public function destroy(string $id, array $options = []): bool
    {
        $canUpdate = $this->canUpdateHoliday($id);
        if (!$canUpdate) {
            throw new NicoBadRequestException(trans('responses.holiday.passed'), 'err_holiday_passed');
        }
        return parent::destroy($id);
    }

    /**
     * @param string $id
     * @return bool
     */
    private function canUpdateHoliday(string $id): bool
    {
        $holiday = parent::getById($id);
        if ($holiday->date < Carbon::today()->toDateString()) {
            return false;
        }

        return true;
    }
}
