<?php


namespace App\Events\User;


class UserLoginLogout
{
    const USER_LOGOUT = 'users.logout';

    const USER_LOGIN = 'users.login';

    /**
     * @param string $message
     */
    public function __construct(public string $message)
    {
    }
}
