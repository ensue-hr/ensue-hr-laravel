<?php

namespace App\Events\User;

use App\System\User\Database\Models\User;

class UserSocialiteLoggedIn
{
    /**
     * @param string $message
     * @param \App\System\User\Database\Models\User $user
     */
    public function __construct(public string $message, public User $user)
    {
    }
}
