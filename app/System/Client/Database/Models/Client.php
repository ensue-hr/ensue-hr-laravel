<?php
/**
 * Created by PhpStorm.
 * User: Amar
 * Date: 1/8/2017
 * Time: 2:13 PM
 */

namespace App\System\Client\Database\Models;

use App\System\AppBaseModel;
use App\System\Project\Database\Models\Project;
use Database\Factories\ClientFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Client extends AppBaseModel
{
    /**
     * @var array
     */
    protected $fillable = ['name', 'avatar'];

    /**
     * @var string
     */
    protected string $defaultSortColumn = 'name';

    /**
     * @var array|string[]
     */
    protected array $sortableColumns = ['name', 'created_at'];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @return \Database\Factories\ClientFactory
     */
    protected static function newFactory(): ClientFactory
    {
        return ClientFactory::new();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function projects(): HasMany
    {
        return $this->hasMany(Project::class, 'client_id', 'id');
    }
}
