<?php


namespace App\System\Setting\Database\Traits;


use App\System\Setting\Database\Models\Setting;
use App\System\Setting\Foundation\SettingDefaultValue;
use App\System\Setting\Foundation\SettingKey;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

trait FiscalYearTrait
{
    /**
     * @param bool $currentFiscalYear
     * @return array
     */
    public function getStartAndEndFiscalDate(bool $currentFiscalYear = true): array
    {
        $fiscalDateStartSetting = Setting::where('key', SettingKey::SYSTEM_FISCAL_DATE_START)->first()?->value ?? SettingDefaultValue::SYSTEM_FISCAL_DATE_START_VALUE;
        $fiscalDateStart = Carbon::now()->year . '-' . ($fiscalDateStartSetting);
        if ($fiscalDateStart > Carbon::now()->toDateString()) {
            $fiscalDateStart = Carbon::now()->subYear()->year . '-' . ($fiscalDateStartSetting);
        }
        $fiscalDateEnd = Carbon::parse($fiscalDateStart)->addYear()->subDay()->toDateString();

        return [
            'start_date' => $currentFiscalYear ? $fiscalDateStart : Carbon::parse($fiscalDateStart)->addYear()->toDateString(),
            'end_date' => $currentFiscalYear ? $fiscalDateEnd : Carbon::parse($fiscalDateEnd)->addYear()->toDateString(),
        ];
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $startDate
     * @param string $endDate
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCurrentFiscalYear(Builder $query, string $startDate = 'start_date', string $endDate = 'end_date'): Builder
    {
        $fiscalDates = $this->getStartAndEndFiscalDate(true);

        return $query->whereDate($startDate, '>=', $fiscalDates['start_date'])
            ->whereDate($endDate, '<=', $fiscalDates['end_date']);
    }
}
