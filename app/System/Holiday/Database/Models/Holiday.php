<?php
/**
 * Created by PhpStorm.
 * User: Amar
 * Date: 1/8/2017
 * Time: 2:13 PM
 */

namespace App\System\Holiday\Database\Models;

use App\System\AppBaseModel;
use Database\Factories\HolidayFactory;

class Holiday extends AppBaseModel
{
    /**
     * @var array
     */
    protected $fillable = ['title', 'date', 'remarks'];

    protected array $sortableColumns = ['title', 'created_at', 'date'];

    /**
     * @return \Database\Factories\HolidayFactory
     */
    protected static function newFactory(): HolidayFactory
    {
        return HolidayFactory::new();
    }
}
