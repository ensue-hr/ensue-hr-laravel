<?php

namespace App\System\Common\Database\Models;

use App\System\AppBaseModel;
use Database\Factories\AddressFactory;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use NicoSystem\Foundation\Status;

class Address extends AppBaseModel
{
    /**
     * @var string[]
     */
    protected $fillable = ['country', 'state', 'city', 'street', 'zip_code', 'type', 'status', 'addressable_id', 'addressable_type'];

    /**
     * @var array
     */
    protected $attributes = [
        'status' => Status::STATUS_PUBLISHED,
    ];

    /**
     * Address constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->makeHidden(['status']);
    }

    /**
     * @var string
     */
    protected string $defaultSortColumn = 'type';

    /**
     * @return MorphTo
     */
    public function addressable(): MorphTo
    {
        return $this->morphTo();
    }

    /**
     * @return AddressFactory
     */
    protected static function newFactory(): AddressFactory
    {
        return new AddressFactory();
    }

}
