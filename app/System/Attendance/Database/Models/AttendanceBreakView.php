<?php

namespace App\System\Attendance\Database\Models;

use App\System\Foundation\Scope\AddEmployeeScopeTrait;
use NicoSystem\Foundation\Database\BaseModel;

class AttendanceBreakView extends BaseModel
{
    use AddEmployeeScopeTrait;

    protected $table = 'vw_attendance_breaks';
    
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->globalQueryName = 'employee_relation';
    }

    /**
     * @param int|null $unixTimestamp
     *
     * @return string|null
     */
    public function getBreakOutAttribute(?int $unixTimestamp): string|null
    {
        if (!is_null($unixTimestamp)) {
            return gmdate("Y-m-d H:i:s", $unixTimestamp);
        }
        return null;
    }

    /**
     * @param int|null $unixTimestamp
     *
     * @return string|null
     */
    public function getBreakInAttribute(?int $unixTimestamp): string|null
    {
        if (!is_null($unixTimestamp)) {
            return gmdate("Y-m-d H:i:s", $unixTimestamp);
        }
        return null;
    }
}
