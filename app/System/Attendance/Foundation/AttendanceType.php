<?php

namespace App\System\Attendance\Foundation;

class AttendanceType
{
    const CHECK_IN = 'check_in';

    const CHECK_OUT = 'check_out';

    const BREAK_OUT = 'break_out';

    const BREAK_IN = 'break_in';

    const LEAVE_IN = 'leave_in';
    
    const LEAVE_OUT = 'leave_out';

    /**
     * @return string[]
     */
    public static function options(): array
    {
        return [
            self::CHECK_IN,
            self::CHECK_OUT,
            self::BREAK_OUT,
            self::BREAK_IN,
        ];
    }

}
