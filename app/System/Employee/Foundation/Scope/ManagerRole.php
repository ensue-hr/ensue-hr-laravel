<?php

namespace App\System\Employee\Foundation\Scope;

use App\System\User\Foundation\RoleName;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class ManagerRole implements Scope
{

    /**
     * @param Builder $builder
     * @param Model $model
     *
     * @return Builder
     */
    public function apply(Builder $builder, Model $model)
    {
        return $builder->whereHas('user.roles', function (Builder $builder) {
            $builder->where('name', RoleName::MANAGER);
        });
    }
}
