<?php

namespace App\System\User\Database\Models;

use App\System\Department\Database\Models\Department;
use App\System\Employee\Database\Models\Employee;
use App\System\Employee\Database\Models\EmployeeView;
use App\System\Foundation\Scope\GlobalEmployeeScope;
use App\System\UserPreference\Database\Models\UserPreference;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class User
 *
 * @package App\System\User\Database\Models
 */
class User extends AbstractUser
{
    const STATUS_LOCKED = 1;

    const STATUS_UNLOCKED = 0;

    /**
     * @var string[]
     */
    protected $appends = [
        'name'
    ];

    public function getNameAttribute(): string
    {
        return preg_replace('/\s+/', ' ', $this->first_name.' '.$this->middle_name.' '.$this->last_name);
    }

    /**
     * @return HasMany
     */
    public function providers(): HasMany
    {
        return $this->hasMany(Provider::class, 'user_id', 'id');
    }

    /**
     * @return HasOne
     */
    public function employee(): HasOne
    {
        return $this->hasOne(Employee::class, 'user_id', 'id')->withoutGlobalScope(GlobalEmployeeScope::class);
    }

    /**
     * @return HasOne
     */
    public function employeeView(): HasOne
    {
        return $this->hasOne(EmployeeView::class, 'user_id', 'id')->withoutGlobalScope(GlobalEmployeeScope::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function preferences(): HasMany
    {
        return $this->hasMany(UserPreference::class, 'user_id', 'id');
    }
}
