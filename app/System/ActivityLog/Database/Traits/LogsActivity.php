<?php


namespace App\System\ActivityLog\Database\Traits;


use App\System\ActivityLog\ActivityLogger;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;


trait LogsActivity
{
    use HasChanges;

    public static bool $recordActivity = true;

    /**
     * set activity should be record or not
     *
     * @param bool $flag
     * @return Model
     */
    public static function setActivityRecord(bool $flag): Model
    {
        static::$recordActivity = $flag;
        return new static();
    }

    /**
     * boot logs activity
     */
    protected static function bootLogsActivity(): void
    {
        static::actionToRecorded()->each(function (string $action) {
            static::$action(function (Model $model) use ($action) {

                if (static::$recordActivity) {
                    $className = $model->getMorphClass();
                    $title = Str::plural(Str::lower($className)) . '.' . $action;
                    $attribute = $model->attributeValuesToLogged($action);

                    //log data
                    $logger = new ActivityLogger();
                    $logger->setData($attribute)
                        ->setTitle($title)
                        ->setModel($model->getMorphClass())
                        ->setModelId($model->id)
                        ->saveLog();
                }
            });
        });
    }


    /**
     * @return \Illuminate\Support\Collection
     */
    public static function actionToRecorded(): Collection
    {
        return collect([
            'created',
            'updated',
            'deleted',
        ]);
    }
}
