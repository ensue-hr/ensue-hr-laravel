<?php
/**
 * Created by PhpStorm.
 * User: Amar
 * Date: 1/8/2017
 * Time: 2:13 PM
 */

namespace App\System\ActivityLog\Database\Models;

use App\System\User\Database\Models\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use NicoSystem\Foundation\Database\BaseModel;
use NicoSystem\Foundation\Database\EditorLogs;


class ActivityLog extends BaseModel
{
    use SoftDeletes, EditorLogs;

    protected $fillable = ['title', 'user_id', 'modular_type', 'modular_id', 'data'];

    protected $hidden = ['user_id', 'modular_type', 'modular_id', 'deleted_at', 'created_by', 'updated_by', 'deleted_by'];

    /**
     * @var array
     */
    protected $casts = [
        'data' => 'array'
    ];

    /**
     * @var string
     */
    protected string $defaultSortColumn = 'created_at';

    /**
     * @var string
     */
    protected string $defaultSortOrder = 'desc';

    /**
     * @var string[]
     */
    protected $appends = [
        'description',
    ];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @return string
     */
    public function getDescriptionAttribute(): string
    {
        return trans('activity_log.' . $this->title, ['user' => $this->user->name]);
    }

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function model(): MorphTo
    {
        return $this->morphTo(null, 'modular_type', 'modular_id')->withTrashed();
    }

    /**
     * Tell what model should the model use while updating/creating/deleting.
     * Example return Auth::user()
     *
     * @return Authenticatable|null
     */
    public function editorProvider(): ?Authenticatable
    {
        return Auth::user();
    }

    /**
     * What model to use if editor log is enabled. example User::class
     *
     * @return string
     */
    public function getEditorModelClassName(): string
    {
        return User::class;
    }
}
