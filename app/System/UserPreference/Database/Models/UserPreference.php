<?php
/**
 * Created by PhpStorm.
 * User: Amar
 * Date: 1/8/2017
 * Time: 2:13 PM
 */

namespace App\System\UserPreference\Database\Models;

use App\System\AppBaseModel;
use App\System\User\Database\Models\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UserPreference extends AppBaseModel
{
    /**
     * @var array
     */
    protected $fillable = ['key', 'value', 'category'];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @var string
     */
    protected string $defaultSortColumn = 'key';

    /**
     * @var array|string[]
     */
    protected array $sortableColumns = ['key', 'category', 'created_at'];

    protected static function newFactory(): void
    {
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
