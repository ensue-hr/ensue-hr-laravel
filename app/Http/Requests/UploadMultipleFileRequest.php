<?php

namespace App\Http\Requests;

use NicoSystem\Requests\NicoRequest;

class UploadMultipleFileRequest extends NicoRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'files' => 'required|array',
            'files.*' => "required|file|mimes:jpg,png,jpeg,gif,svg,pdf,doc,docx,xlsx"
        ];
    }
}
