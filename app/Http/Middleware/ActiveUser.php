<?php

namespace App\Http\Middleware;

use App\Exceptions\UserDeactivatedException;
use Closure;
use Illuminate\Http\Request;

class ActiveUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next): mixed
    {
        if (!$request->user('api')->status) {
            throw new UserDeactivatedException(trans('responses.user.deactivated'));
        }
        return $next($request);
    }
}
