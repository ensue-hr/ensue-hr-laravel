<?php

namespace Database\Factories;

use App\System\Holiday\Database\Models\Holiday;
use Illuminate\Database\Eloquent\Factories\Factory;

class HolidayFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Holiday::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->name,
            'date' => $this->faker->unique()->dateTimeThisYear->format('Y-m-d'),
            'remarks' => null,
        ];
    }
}
