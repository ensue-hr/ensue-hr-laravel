<?php

namespace Database\Factories;

use App\System\Project\Database\Models\Project;
use App\System\Setting\Foundation\SettingDefaultValue;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use NicoSystem\Foundation\Status;

class ProjectFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Project::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->name,
            'description' => $this->faker->paragraph,
            'code' => $this->faker->unique()->title,
            'type' => $this->faker->randomElement(SettingDefaultValue::PROJECT_TYPE_VALUE),
            'avatar' => $this->faker->imageUrl,
            'status' => Status::STATUS_PUBLISHED,
            'start_date' => Carbon::now()->toDateString(),
            'end_date' => null,
        ];
    }
}
