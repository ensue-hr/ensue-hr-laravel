<?php

namespace Database\Factories;

use App\System\Leave\Database\Models\Leave;
use App\System\Leave\Foundation\LeaveStatus;
use App\System\Setting\Database\Models\Setting;
use App\System\Setting\Foundation\SettingKey;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class LeaveFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Leave::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $type = Setting::where('key', SettingKey::LEAVES_TYPE)->first();

        return [
            'title' => $this->faker->sentence(5),
            'description' => $this->faker->paragraph(5),
            'start_at' => Carbon::today()->addDays(7)->toDateTimeString(),
            'end_at' => Carbon::today()->addDays(9)->subSecond()->toDateTimeString(),
            'days' => 2,
            'type' => $this->faker->randomElement($type->value),
            'status' => LeaveStatus::PENDING,
            'is_emergency' => false,
        ];
    }
}
