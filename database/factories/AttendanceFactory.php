<?php

namespace Database\Factories;

use App\System\Attendance\Database\Models\Attendance;
use App\System\Attendance\Foundation\AttendanceType;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use NicoSystem\Foundation\Status;

class AttendanceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Attendance::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'attend_at' => Carbon::now()->toDateTimeString(),
            'type' => AttendanceType::CHECK_IN,
            'punch_count' => 1,
            'browser' => $this->faker->randomElement(['chrome', 'safari', 'firefox']),
            'device' => $this->faker->randomElement(['windows', 'ubuntu', 'app']),
            'location' => $this->faker->address,
            'ip' => $this->faker->ipv6,
            'fingerprint' => random_int(1000000, 9999999),
            'status' => Status::STATUS_PUBLISHED,
        ];
    }
}
