<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttendancesTable extends Migration
{
    use \Database\Traits\EditorTrait;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendances', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamp('attend_at');
            $table->string('type', 50);
            $table->text('reason')->nullable();
            $table->string('browser', 50)->nullable();
            $table->string('device', 100)->nullable();
            $table->string('location', 100)->nullable();
            $table->ipAddress('ip')->nullable();
            $table->boolean('status');
            $table->unsignedBigInteger('employee_id');
            $table->unsignedBigInteger('parent_id')->nullable();
            $this->editorLogs($table);

            $table->foreign('employee_id')->references('id')->on('employees')->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('parent_id')->references('id')->on('attendances')->onUpdate('cascade')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('attendances');
        Schema::enableForeignKeyConstraints();
    }
}
