<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateVwAttendancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('drop view if exists vw_attendances');

        if (config('database.default') == 'sqlite') {
            $attendDate = "strftime('%Y-%m-%d', datetime(attendances.attend_at / 1000, 'unixepoch'))";
            $checkTimeDiff = "MAX(CASE WHEN TYPE = 'check_out' THEN attend_at END) - MAX(CASE WHEN TYPE = 'check_in' THEN attend_at END)";
            $leaveTimeDiff = "MAX(CASE WHEN TYPE = 'leave_out' THEN attend_at END) - MAX(CASE WHEN TYPE = 'leave_in' THEN attend_at END)";
            $sumOfBreaks = "SUM(break_time)";
        } else {
            $attendDate = "DATE(attendances.attend_at)";
            $checkTimeDiff = "TIMEDIFF(MAX(CASE WHEN TYPE = 'check_out' THEN attend_at END),
                        MAX(CASE WHEN TYPE = 'check_in' THEN attend_at END))";
            $leaveTimeDiff = "TIMEDIFF(MAX(CASE WHEN TYPE = 'leave_out' THEN attend_at END),
                        MAX(CASE WHEN TYPE = 'leave_in' THEN attend_at END))";
            $sumOfBreaks = "SEC_TO_TIME(SUM(TIME_TO_SEC(break_time)))";
        }

        $break_hours = DB::table('vw_attendance_breaks')
            ->select('attend_date',
                DB::raw("${sumOfBreaks} as break_time"),
                'employee_id'
            )->groupBy('attend_date', 'employee_id');

        $sqlQuery = DB::table('attendances')
            ->select(
                'attendances.attend_at',
                'attendances.employee_id',
                'vw_employees.name',
                'vw_employees.email',
                'vw_employees.avatar',
                'vw_employees.code',
                DB::raw(
                    "{$attendDate} AS attend_date,
                        MAX(CASE WHEN TYPE = 'check_in' THEN attend_at END)           AS check_in,
                        MAX(CASE WHEN TYPE = 'check_out' THEN attend_at END)          AS check_out,
                        MAX(CASE WHEN TYPE = 'leave_in' THEN attend_at END)           AS leave_in,
                        MAX(CASE WHEN TYPE = 'leave_out' THEN attend_at END)          AS leave_out,
                        {$checkTimeDiff} as work_time,
                        b.break_time,
                        {$leaveTimeDiff} as leave_time"
                ))
            ->join('vw_employees', 'vw_employees.id', '=', 'attendances.employee_id')
            ->join(DB::raw("({$break_hours->toSql()}) b"), function ($join) use ($attendDate) {
                $join->on('b.attend_date', '=', DB::raw($attendDate))
                    ->on('b.employee_id', '=', 'attendances.employee_id');
            })->whereNull('attendances.deleted_at')
            ->whereRaw('attendances.status = 2')
            ->groupBy('attendances.employee_id', DB::raw($attendDate));

        $sqlQuery = array_reduce($sqlQuery->getBindings(), function ($sql, $binding) {
            return preg_replace('/\?/', is_numeric($binding) ? $binding : "'" . $binding . "'", $sql, 1);
        }, $sqlQuery->toSql());

        DB::statement('Create view vw_attendances as ' . $sqlQuery);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('drop view if exists vw_attendances');
    }
}
