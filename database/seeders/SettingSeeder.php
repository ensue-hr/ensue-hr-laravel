<?php

namespace Database\Seeders;

use App\System\Setting\Database\Models\Setting;
use App\System\Setting\Foundation\SettingCategory;
use App\System\Setting\Foundation\SettingDefaultValue;
use App\System\Setting\Foundation\SettingKey;
use App\System\Setting\Foundation\SettingType;
use App\System\UserPreference\Foundation\UserPreferenceKey;
use App\System\UserPreference\Foundation\UserPreferenceValue;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $settings = [
            SettingKey::LEAVES_TYPE => [
                'value' => SettingDefaultValue::LEAVES_TYPE,
                'type' => SettingType::ARRAY,
                'category' => SettingCategory::LEAVES,
            ],
            SettingKey::LEAVES_MAX_REQUEST_DAYS => [
                'value' => SettingDefaultValue::LEAVES_MAX_REQUEST_DAYS_VALUE,
                'type' => SettingType::INTEGER,
                'category' => SettingCategory::LEAVES,
            ],
            SettingKey::LEAVES_CAN_REQUEST_BEFORE_DAYS => [
                'value' => SettingDefaultValue::LEAVES_CAN_REQUEST_BEFORE_DAYS_VALUE,
                'type' => SettingType::INTEGER,
                'category' => SettingCategory::LEAVES,
            ],
            SettingKey::SYSTEM_FISCAL_DATE_START => [
                'value' => SettingDefaultValue::SYSTEM_FISCAL_DATE_START_VALUE,
                'type' => SettingType::STRING,
                'category' => SettingCategory::SYSTEM,
            ],
            SettingKey::ATTENDANCE_EMPLOYEE_UPDATE_ENABLE => [
                'value' => SettingDefaultValue::ATTENDANCE_EMPLOYEE_UPDATE_ENABLE_VALUE,
                'type' => SettingType::BOOLEAN,
                'category' => SettingCategory::ATTENDANCES,
            ],
            SettingKey::ATTENDANCE_EMPLOYEE_UPDATE_BEFORE_DAYS => [
                'value' => SettingDefaultValue::ATTENDANCE_EMPLOYEE_UPDATE_BEFORE_DAYS_VALUE,
                'type' => SettingType::INTEGER,
                'category' => SettingCategory::ATTENDANCES,
            ],
            SettingKey::SYSTEM_WEEKENDS => [
                'value' => SettingDefaultValue::SYSTEM_WEEKENDS_VALUE,
                'type' => SettingType::ARRAY,
                'category' => SettingCategory::SYSTEM,
            ],
            UserPreferenceKey::EMPLOYEE_WORK_TIME => [
                'value' => UserPreferenceValue::EMPLOYEE_WORK_TIME_VALUE,
                'type' => SettingType::STRING,
                'category' => SettingCategory::SYSTEM,
            ],
            UserPreferenceKey::USER_TIMEZONE => [
                'value' => UserPreferenceValue::USER_TIMEZONE_VALUE,
                'type' => SettingType::STRING,
                'category' => SettingCategory::SYSTEM,
            ],
            UserPreferenceKey::USER_THEME => [
                'value' => UserPreferenceValue::LIGHT_THEME,
                'type' => SettingType::STRING,
                'category' => SettingCategory::UI,
            ],
            UserPreferenceKey::USER_LIST_VIEW => [
                'value' => UserPreferenceValue::GRID_VIEW,
                'type' => SettingType::STRING,
                'category' => SettingCategory::UI,
            ],
            SettingKey::DEFAULT_EMAIL_GROUPS => [
                'value' => SettingDefaultValue::DEFAULT_EMAIL_GROUPS_VALUE,
                'type' => SettingType::ARRAY,
                'category' => SettingCategory::SYSTEM,
            ],
            SettingKey::PROJECT_TYPE => [
                'value' => SettingDefaultValue::PROJECT_TYPE_VALUE,
                'type' => SettingType::ARRAY,
                'category' => SettingCategory::PROJECTS,
            ],
            SettingKey::ATTENDANCE_EMPLOYEE_WORKING_HOURS => [
                'value' => SettingDefaultValue::ATTENDANCE_EMPLOYEE_WORKING_HOURS_VALUE,
                'type' => SettingType::INTEGER,
                'category' => SettingCategory::ATTENDANCES,
            ],
        ];

        foreach ($settings as $key => $setting) {
            if (is_array($setting['value'])) {
                $setting['value'] = implode('|', $setting['value']);
            }
            Setting::setActivityRecord(false)->firstOrCreate(['key' => $key], $setting);
            Setting::setActivityRecord(false)->updateOrCreate(['key' => $key], Arr::only($setting, ['type', 'category']));
        }
    }
}
