<?php

namespace Database\Seeders;

use App\System\Employee\Database\Models\Employee;
use App\System\User\Database\Models\User;
use App\System\User\Foundation\RoleName;
use Exception;
use Illuminate\Database\Seeder;

class DefaultAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run(): void
    {
        $superUser = User::setActivityRecord(false)->firstOrCreate(
            ['email' => 'superuser@ensue.us'],
            [
                'first_name' => 'super',
                'last_name' => 'admin',
                'email_verified_at' => now(),
                'password' => bcrypt('hrApp@2021'),
                'status' => User::STATUS_ACTIVE,
                'is_locked' => User::STATUS_UNLOCKED,
                'avatar' => 'http://lorempixel.com/400/200/people/' . random_int(1, 10),
            ]
        );

        $superUser->assignRole(RoleName::MANAGER);

        $employees = Employee::all();
        $employees->each(function ($employee) {
            $employee->user->assignRole(RoleName::EMPLOYEE);
        });
    }
}
