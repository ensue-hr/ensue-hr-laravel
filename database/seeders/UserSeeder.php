<?php

namespace Database\Seeders;

use App\System\User\Database\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        User::setActivityRecord(false)->firstOrCreate(
            ['email' => 'shiva.thapa@ensue.us'],
            [
                'first_name' => 'shiva',
                'last_name' => 'thapa',
                'email_verified_at' => now(),
                'password' => bcrypt('shiva@hr'),
                'status' => User::STATUS_ACTIVE,
                'is_locked' => User::STATUS_UNLOCKED,
            ]
        );

        User::setActivityRecord(false)->firstOrCreate(
            ['email' => 'bipin.maharjan@ensue.us'],
            [
                'first_name' => 'bipin',
                'last_name' => 'maharjan',
                'email_verified_at' => now(),
                'password' => bcrypt('bipin@hr'),
                'status' => User::STATUS_ACTIVE,
                'is_locked' => User::STATUS_UNLOCKED,
            ]
        );
    }
}
