<?php

namespace Database\Traits;

use Illuminate\Database\Schema\Blueprint;

/**
 * Created by PhpStorm.
 * User: amar
 * Date: 5/22/18
 * Time: 2:01 PM
 */
trait EditorTrait
{
    protected function editorLogs(Blueprint $blueprint)
    {
        $blueprint->unsignedBigInteger('created_by')->nullable();
        $blueprint->unsignedBigInteger('updated_by')->nullable();
        $blueprint->unsignedBigInteger('deleted_by')->nullable();

        $blueprint->foreign('created_by')->references('id')->on('users');
        $blueprint->foreign('updated_by')->references('id')->on('users');
        $blueprint->foreign('deleted_by')->references('id')->on('users');

        $blueprint->timestamps();
        $blueprint->softDeletes();
    }
}
