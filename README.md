# Ensue HR App
Technologies used:
* PHP V 8.0 [Framework: Laravel]
* Javascript [Framework: Angular]

### Project setup from docker 
1. Copy docker\.env.example to docker\.env
2. In order run project: goto {projectDirectory}\docker in terminal, run `docker-compose up`
3. To rebuild and rerun the project, run `docker-compose up --build`

### Project setup step by step
* Copy env.example file to new file named .env
* From the project root run `composer install` command to install dependencies.
* Run `php artisan migrate` command within the project root to initialize database.
* Run `php artisan db:seed` to run necessary seeds required by the application
* Run `php artisan serve` to run the application
