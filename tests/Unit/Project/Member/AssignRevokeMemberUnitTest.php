<?php

namespace Project\Member;

use App\Modules\Projects\Database\Models\Project;
use App\Modules\Projects\Database\Models\ProjectRole;
use App\Modules\Projects\Mails\ProjectAssignMemberMail;
use App\Modules\Projects\Mails\ProjectRevokeMemberMail;
use App\Modules\Projects\Repositories\MemberRepository;
use App\System\Employee\Database\Models\Employee;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class AssignRevokeMemberUnitTest extends TestCase
{
    /**
     * @var \App\Modules\Projects\Repositories\MemberRepository
     */
    private MemberRepository $repository;

    private \App\System\Project\Database\Models\Project $project;

    public function testMemberShouldBeAdded(): void
    {
        $employeeIds = Employee::factory()->forUser()->forDepartment()->count(5)->create()->pluck('id');
        $data = [
            'role_id' => ProjectRole::factory()->create()->id,
            'employee_ids' => $employeeIds,
            'licensed' => true
        ];
        $this->repository->assignMembers($this->project->id, $data);

        $employeeIds->each(function (int $id) use ($data) {
            $assertData = array_merge(['employee_id' => $id, 'project_id' => $this->project->id], Arr::only($data, ['role_id', 'licensed']));
            $this->assertDatabaseHas('project_employees', $assertData);
        });
    }

    public function testMemberShouldNotBeAddedForUnknownProject(): void
    {
        $employeeIds = Employee::factory()->forUser()->forDepartment()->count(5)->create()->pluck('id');
        $data = [
            'role_id' => ProjectRole::factory()->create()->id,
            'employee_ids' => $employeeIds,
            'licensed' => true
        ];
        $this->expectException(ModelNotFoundException::class);
        $this->repository->assignMembers(random_int(1000, 2000), $data);
    }

    public function testMailShouldBeSentWhenMemberIsAdded(): void
    {
        Mail::fake();
        $employeeIds = Employee::factory()->forUser()->forDepartment()->count(2)->create()->pluck('id');
        $data = [
            'role_id' => ProjectRole::factory()->create()->id,
            'employee_ids' => $employeeIds,
            'licensed' => true
        ];
        $this->repository->assignMembers($this->project->id, $data);
        Mail::assertQueued(ProjectAssignMemberMail::class);
    }

    public function testMemberShouldBeRevoked(): void
    {
        $employee = Employee::factory()->forUser()->forDepartment()->create();
        $role = ProjectRole::factory()->create();
        $this->project->employees()->syncWithoutDetaching([$employee->id => ['role_id' => $role->id, 'licensed' => true]]);
        $this->repository->revokeMember($this->project->id, $employee->id);
        $this->assertDatabaseHas('project_employees', [
            'employee_id' => $employee->id,
            'project_id' => $this->project->id,
            'role_id' => $role->id,
            'licensed' => false
        ]);
    }

    public function testMemberShouldNotBeRevokedForUnknownProject(): void
    {
        $employee = Employee::factory()->forUser()->forDepartment()->create();
        $role = ProjectRole::factory()->create();
        $this->project->employees()->syncWithoutDetaching([$employee->id => ['role_id' => $role->id, 'licensed' => true]]);
        $this->expectException(ModelNotFoundException::class);
        $this->repository->revokeMember(random_int(1000, 2000), $employee->id);
    }

    public function testMemberShouldNotBeRevokedForUnlicensedEmployee(): void
    {
        $employee = Employee::factory()->forUser()->forDepartment()->create();
        $role = ProjectRole::factory()->create();
        $this->project->employees()->syncWithoutDetaching([$employee->id => ['role_id' => $role->id, 'licensed' => false]]);
        $this->expectException(ModelNotFoundException::class);
        $this->repository->revokeMember($this->project->id, $employee->id);
    }

    public function testMemberShouldNotBeRevokedForUnknownEmployee(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->revokeMember($this->project->id, random_int(1000, 2000));
    }

    public function testMailShouldBeSentWhenProjectIsRevoked(): void
    {
        Mail::fake();
        $employee = Employee::factory()->forUser()->forDepartment()->create();
        $role = ProjectRole::factory()->create();
        $this->project->employees()->syncWithoutDetaching([$employee->id => ['role_id' => $role->id, 'licensed' => true]]);
        $this->repository->revokeMember($this->project->id, $employee->id);
        Mail::assertQueued(ProjectRevokeMemberMail::class);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new MemberRepository(new Project());
        $this->project = Project::factory()->forClient()->create();
    }

}
