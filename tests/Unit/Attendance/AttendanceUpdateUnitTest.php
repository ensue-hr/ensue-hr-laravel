<?php

namespace Attendance;

use App\Modules\Attendances\Database\Models\Attendance;
use App\Modules\Attendances\Database\Models\Setting;
use App\Modules\Attendances\Exceptions\AttendanceIntersectException;
use App\Modules\Attendances\Exceptions\AttendanceUpdateDisableException;
use App\Modules\Attendances\Exceptions\AttendanceUpdateTimeExceedException;
use App\Modules\Attendances\Exceptions\DifferentAttendanceDateException;
use App\Modules\Attendances\Exceptions\SameTimeAttendedException;
use App\Modules\Attendances\Mails\AttendanceUpdatedMail;
use App\Modules\Attendances\Repositories\AttendanceRepository;
use App\System\Attendance\Foundation\AttendanceType;
use App\System\Employee\Database\Models\Employee;
use App\System\Setting\Foundation\SettingKey;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Mail;
use NicoSystem\Exceptions\NicoBadRequestException;
use NicoSystem\Foundation\Status;
use Tests\TestCase;

class AttendanceUpdateUnitTest extends TestCase
{
    private AttendanceRepository $repository;

    private Employee $employee;

    public function testEmployeeCannotUpdateAttendanceIfDisabled(): void
    {
        $attendance = Attendance::factory()->create([
            'attend_at' => Carbon::today()->subDays(2)->toDateTimeString(),
            'employee_id' => $this->employee->id
        ]);
        $data = [
            'attend_at' => Carbon::today()->subDays(2)->toDateString() . ' 09:00:00',
            'reason' => $this->faker->paragraph,
        ];
        Setting::where('key', SettingKey::ATTENDANCE_EMPLOYEE_UPDATE_ENABLE)->update(['value'=> false]);
        $this->expectException(AttendanceUpdateDisableException::class);
        $this->repository->updateAttendanceByEmployee($this->repository->getAttendanceByIdExceptLeaves($attendance->id), $data);
    }

    public function testEmployeeCannotUpdateAttendanceMoreThanOneDay(): void
    {
        $attendance = Attendance::factory()->create([
            'attend_at' => Carbon::today()->subDays(2)->toDateTimeString(),
            'employee_id' => $this->employee->id
        ]);
        $data = [
            'attend_at' => Carbon::today()->subDays(2)->toDateString() . ' 09:00:00',
            'reason' => $this->faker->paragraph,
        ];
        $this->expectException(AttendanceUpdateTimeExceedException::class);
        $this->repository->updateAttendanceByEmployee($this->repository->getAttendanceByIdExceptLeaves($attendance->id), $data);
    }

    public function testEmployeeCannotUpdateAttendanceForSameAttendAt(): void
    {
        $attendance = Attendance::factory()->create([
            'attend_at' => Carbon::today()->toDateTimeString(),
            'employee_id' => $this->employee->id
        ]);
        $data = [
            'attend_at' => Carbon::today()->toDateTimeString(),
            'reason' => $this->faker->paragraph,
        ];
        $this->expectException(SameTimeAttendedException::class);
        $this->repository->updateAttendanceByEmployee($this->repository->getAttendanceByIdExceptLeaves($attendance->id), $data);
    }

    public function testEmployeeCanUpdateAttendanceTimeLogWithReason(): void
    {
        Attendance::factory()->create([
            'attend_at' => Carbon::yesterday()->subDay()->toDateString() . ' 18:00:00',
            'employee_id' => $this->employee->id,
            'type' => AttendanceType::CHECK_OUT,
        ]);

        $attendance = Attendance::factory()->create([
            'attend_at' => Carbon::yesterday()->toDateTimeString(),
            'employee_id' => $this->employee->id
        ]);

        Attendance::factory()->create([
            'attend_at' => Carbon::yesterday()->toDateString() . ' 23:59:59',
            'employee_id' => $this->employee->id,
            'type' => AttendanceType::CHECK_OUT,
        ]);
        $data = Attendance::factory()->raw([
            'attend_at' => Carbon::yesterday()->toDateString() . ' 09:00:00',
            'reason' => $this->faker->paragraph,
        ]);
        $result = $this->repository->updateAttendanceByEmployee($this->repository->getAttendanceByIdExceptLeaves($attendance->id), $data);
        $this->assertInstanceOf(Attendance::class, $result);
        $this->assertEquals($data['attend_at'], $result->attend_at);
        $this->assertEquals($attendance->type, $result->type);
        $this->assertEquals($attendance->id, $result->parent_id);
        $this->assertEquals($data['reason'], $result->reason);
        $this->assertEquals($data['browser'], $result->browser);
        $this->assertEquals($data['device'], $result->device);
        $this->assertEquals($data['location'], $result->location);
        $this->assertEquals($data['ip'], $result->ip);
        $this->assertEquals(Status::STATUS_PUBLISHED, $result->status);
        $attendance = $attendance->fresh()->only('id', 'attend_at', 'type', 'reason', 'status');
        $attendance['status'] = Status::STATUS_UNPUBLISHED;
        $attendance['reason'] = null;
        $this->assertDatabaseHas('attendances', $attendance);
    }

    public function testEmployeeCannotUpdateAttendanceAffectingOtherAttendance(): void
    {
        $attendance = Attendance::factory()->create([
            'attend_at' => Carbon::yesterday()->toDateTimeString(),
            'employee_id' => $this->employee->id
        ]);

        Attendance::factory()->create([
            'attend_at' => Carbon::yesterday()->toDateString() . ' 18:00:00',
            'employee_id' => $this->employee->id,
            'type' => AttendanceType::CHECK_OUT,
        ]);
        $data = [
            'attend_at' => Carbon::yesterday()->toDateString() . ' 18:00:00',
            'reason' => $this->faker->paragraph,
        ];
        $this->expectException(AttendanceIntersectException::class);
        $result = $this->repository->updateAttendanceByEmployee($this->repository->getAttendanceByIdExceptLeaves($attendance->id), $data);
    }

    public function testEmployeeCanUpdateAttendanceWhenPreviousAttendanceIsMissing(): void
    {
        $attendance = Attendance::factory()->create([
            'attend_at' => Carbon::yesterday()->toDateTimeString(),
            'employee_id' => $this->employee->id
        ]);

        Attendance::factory()->create([
            'attend_at' => Carbon::yesterday()->toDateString() . ' 23:59:59',
            'employee_id' => $this->employee->id,
            'type' => AttendanceType::CHECK_OUT,
        ]);
        $data = [
            'attend_at' => Carbon::yesterday()->toDateString() . ' 09:00:00',
            'reason' => $this->faker->paragraph,
        ];
        $result = $this->repository->updateAttendanceByEmployee($this->repository->getAttendanceByIdExceptLeaves($attendance->id), $data);
        $this->assertInstanceOf(Attendance::class, $result);
        $this->assertEquals($data['attend_at'], $result->attend_at);
        $this->assertEquals($data['reason'], $result->reason);
        $this->assertEquals(Status::STATUS_PUBLISHED, $result->status);
        $attendance = $attendance->only('id', 'attend_at', 'type', 'status');
        $attendance['status'] = Status::STATUS_UNPUBLISHED;
        $attendance['reason'] = null;
        $this->assertDatabaseHas('attendances', $attendance);
    }

    public function testEmployeeCanUpdateAttendanceWhenNextAttendanceIsMissing(): void
    {
        Attendance::factory()->create([
            'attend_at' => Carbon::yesterday()->toDateTimeString(),
            'employee_id' => $this->employee->id
        ]);

        $attendance = Attendance::factory()->create([
            'attend_at' => Carbon::yesterday()->toDateString() . ' 23:59:59',
            'employee_id' => $this->employee->id,
            'type' => AttendanceType::CHECK_OUT,
        ]);
        $data = [
            'attend_at' => Carbon::yesterday()->toDateString() . ' 09:00:00',
            'reason' => $this->faker->paragraph,
        ];
        $result = $this->repository->updateAttendanceByEmployee($this->repository->getAttendanceByIdExceptLeaves($attendance->id), $data);
        $this->assertInstanceOf(Attendance::class, $result);
        $this->assertEquals($data['attend_at'], $result->attend_at);
        $this->assertEquals($attendance->type, $result->type);
        $this->assertEquals($data['reason'], $result->reason);
        $this->assertEquals(Status::STATUS_PUBLISHED, $result->status);
        $attendance = $attendance->only('id', 'attend_at', 'type', 'status');
        $attendance['status'] = Status::STATUS_UNPUBLISHED;
        $attendance['reason'] = null;
        $this->assertDatabaseHas('attendances', $attendance);
    }

    public function testEmployeeCannotUpdateAttendanceOfTypeLeaveIn(): void
    {
        $attendance = Attendance::factory()->create([
            'attend_at' => Carbon::yesterday()->toDateTimeString(),
            'employee_id' => $this->employee->id,
            'type' => AttendanceType::LEAVE_IN,
        ]);
        $this->expectException(ModelNotFoundException::class);
        $this->repository->updateAttendanceByEmployee($this->repository->getAttendanceByIdExceptLeaves($attendance->id), $data);
    }

    public function testEmployeeCannotUpdateAttendanceOfTypeLeaveOut(): void
    {
        $attendance = Attendance::factory()->create([
            'attend_at' => Carbon::yesterday()->toDateTimeString(),
            'employee_id' => $this->employee->id,
            'type' => AttendanceType::LEAVE_OUT,
        ]);
        $this->expectException(ModelNotFoundException::class);
        $this->repository->updateAttendanceByEmployee($this->repository->getAttendanceByIdExceptLeaves($attendance->id), $data);
    }

    public function testEmployeeCannotUpdateAttendanceForDifferentDate(): void
    {
        $attendance = Attendance::factory()->create([
            'attend_at' => Carbon::yesterday()->toDateTimeString(),
            'employee_id' => $this->employee->id
        ]);

        Attendance::factory()->create([
            'attend_at' => Carbon::yesterday()->toDateString() . ' 23:59:59',
            'employee_id' => $this->employee->id,
            'type' => AttendanceType::CHECK_OUT,
        ]);
        $data = Attendance::factory()->raw([
            'attend_at' => Carbon::yesterday()->subDay()->toDateString() . ' 09:00:00',
            'reason' => $this->faker->paragraph,
        ]);
        $this->expectException(DifferentAttendanceDateException::class);
        $this->repository->updateAttendanceByEmployee($this->repository->getAttendanceByIdExceptLeaves($attendance->id), $data);
    }

    public function testEmployeeCanUpdateAttendanceTwice(): void
    {
        $attendance = Attendance::factory()->create([
            'attend_at' => Carbon::yesterday()->toDateTimeString(),
            'employee_id' => $this->employee->id,
            'status' => Status::STATUS_UNPUBLISHED
        ]);
        $attendance = Attendance::factory()->create([
            'attend_at' => Carbon::yesterday()->toDateString() . ' 09:00:00',
            'reason' => $this->faker->paragraph,
            'employee_id' => $this->employee->id,
            'status' => Status::STATUS_PUBLISHED,
            'parent_id' => $attendance->id
        ]);
        $data = Attendance::factory()->raw([
            'attend_at' => Carbon::yesterday()->toDateString() . ' 11:00:00',
            'reason' => $this->faker->paragraph,
        ]);
        $result = $this->repository->updateAttendanceByEmployee($this->repository->getAttendanceByIdExceptLeaves($attendance->id), $data);
        $this->assertInstanceOf(Attendance::class, $result);
        $this->assertEquals($data['attend_at'], $result->attend_at);
        $this->assertEquals($data['reason'], $result->reason);
        $this->assertEquals(Status::STATUS_PUBLISHED, $result->status);
        $attendance = $attendance->only('id', 'attend_at', 'type', 'status', 'reason');
        $attendance['status'] = Status::STATUS_UNPUBLISHED;
        $this->assertDatabaseHas('attendances', $attendance);
    }

    public function testEmployeeCannotUpdateAttendanceMoreThanTwice(): void
    {
        $attendance = Attendance::factory()->create([
            'attend_at' => Carbon::yesterday()->toDateTimeString(),
            'employee_id' => $this->employee->id,
            'status' => Status::STATUS_UNPUBLISHED
        ]);

        $attendance = Attendance::factory()->create([
            'attend_at' => Carbon::yesterday()->toDateString() . ' 09:00:00',
            'reason' => $this->faker->paragraph,
            'employee_id' => $this->employee->id,
            'status' => Status::STATUS_UNPUBLISHED,
            'parent_id' => $attendance->id,
        ]);
        $attendance = Attendance::factory()->create([
            'attend_at' => Carbon::yesterday()->toDateString() . ' 10:00:00',
            'reason' => $this->faker->paragraph,
            'employee_id' => $this->employee->id,
            'status' => Status::STATUS_PUBLISHED,
            'parent_id' => $attendance->id
        ]);

        $data = Attendance::factory()->raw([
            'attend_at' => Carbon::yesterday()->toDateString() . ' 11:00:00',
            'reason' => $this->faker->paragraph,
        ]);
        $this->expectException(NicoBadRequestException::class);
        $this->repository->updateAttendanceByEmployee($this->repository->getAttendanceByIdExceptLeaves($attendance->id), $data);
    }

    public function testMailShouldBeSentWhenAttendanceIsUpdated(): void
    {
        Mail::fake();
        Attendance::factory()->create([
            'attend_at' => Carbon::yesterday()->subDay()->toDateString() . ' 18:00:00',
            'employee_id' => $this->employee->id,
            'type' => AttendanceType::CHECK_OUT,
        ]);

        $attendance = Attendance::factory()->create([
            'attend_at' => Carbon::yesterday()->toDateTimeString(),
            'employee_id' => $this->employee->id
        ]);

        Attendance::factory()->create([
            'attend_at' => Carbon::yesterday()->toDateString() . ' 23:59:59',
            'employee_id' => $this->employee->id,
            'type' => AttendanceType::CHECK_OUT,
        ]);
        $data = Attendance::factory()->raw([
            'attend_at' => Carbon::yesterday()->toDateString() . ' 09:00:00',
            'reason' => $this->faker->paragraph,
        ]);
        $this->repository->updateAttendanceByEmployee($this->repository->getAttendanceByIdExceptLeaves($attendance->id), $data);
        Mail::assertQueued(AttendanceUpdatedMail::class);
    }

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->employee = Employee::factory()->forUser()->forDepartment()->create();
        $this->repository = new AttendanceRepository(new Attendance(), new Setting());
    }


}
