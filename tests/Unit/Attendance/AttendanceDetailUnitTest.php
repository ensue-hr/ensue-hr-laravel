<?php

namespace Attendance;

use App\Modules\Attendances\Database\Models\Attendance;
use App\Modules\Attendances\Database\Models\Setting;
use App\Modules\Attendances\Repositories\AttendanceRepository;
use App\System\Attendance\Foundation\AttendanceType;
use App\System\Employee\Database\Models\Employee;
use Carbon\Carbon;
use NicoSystem\Foundation\Status;
use Tests\TestCase;

class AttendanceDetailUnitTest extends TestCase
{

    private AttendanceRepository $repository;

    private Employee $employee;

    public function testAttendanceDetailShouldBeShownForToday(): void
    {
        Attendance::factory()->create([
            'attend_at' => Carbon::today()->toDateString() . ' 09:00:00',
            'employee_id' => $this->employee->id
        ]);
        Attendance::factory()->create([
            'type' => AttendanceType::BREAK_OUT,
            'attend_at' => Carbon::today()->toDateString() . ' 01:00:00',
            'employee_id' => $this->employee->id
        ]);
        Attendance::factory()->create([
            'type' => AttendanceType::BREAK_IN,
            'attend_at' => Carbon::today()->toDateString() . ' 02:00:00',
            'employee_id' => $this->employee->id
        ]);
        Attendance::factory()->create([
            'type' => AttendanceType::CHECK_OUT,
            'attend_at' => Carbon::today()->toDateString() . ' 18:00:00',
            'employee_id' => $this->employee->id
        ]);

        $result = $this->repository->getDetails(['employee_id' => $this->employee->id, 'date' => Carbon::today()->toDateString()]);
        $this->assertCount(4, $result);
    }

    public function testAttendanceDetailShouldBeShownAlongWithItsParents(): void
    {
        $attendance = Attendance::factory()->create([
            'attend_at' => Carbon::today()->toDateString() . ' 09:00:00',
            'employee_id' => $this->employee->id,
            'status' => Status::STATUS_UNPUBLISHED,
        ]);
        $attendance = Attendance::factory()->create([
            'attend_at' => Carbon::today()->toDateString() . ' 01:00:00',
            'employee_id' => $this->employee->id,
            'reason' => $this->faker->paragraph,
            'status' => Status::STATUS_UNPUBLISHED,
            'parent_id' => $attendance->id
        ]);
        $attendance = Attendance::factory()->create([
            'attend_at' => Carbon::today()->toDateString() . ' 02:00:00',
            'employee_id' => $this->employee->id,
            'reason' => $this->faker->paragraph,
            'status' => Status::STATUS_PUBLISHED,
            'parent_id' => $attendance->id
        ]);
        Attendance::factory()->create([
            'type' => AttendanceType::CHECK_OUT,
            'attend_at' => Carbon::today()->toDateString() . ' 18:00:00',
            'employee_id' => $this->employee->id
        ]);
        $result = $this->repository->getDetails(['employee_id' => $this->employee->id, 'date' => Carbon::today()->toDateString()]);
        $this->assertCount(2, $result);
        $this->assertCount(2, $result->firstWhere('id', $attendance->id)->parents);
    }

    public function testAttendanceDetailShouldBeEmptyForDifferentEmployee(): void
    {
        Attendance::factory()->create([
            'attend_at' => Carbon::today()->toDateString() . ' 09:00:00',
            'employee_id' => $this->employee->id
        ]);
        Attendance::factory()->create([
            'type' => AttendanceType::CHECK_OUT,
            'attend_at' => Carbon::today()->toDateString() . ' 18:00:00',
            'employee_id' => $this->employee->id
        ]);

        $result = $this->repository->getDetails(['employee_id' => random_int(1000, 2000), 'date' => Carbon::today()->toDateString()]);
        $this->assertCount(0, $result);
    }

    public function testAttendanceDetailShouldBeEmptyForFutureDate(): void
    {
        Attendance::factory()->create([
            'attend_at' => Carbon::today()->toDateString() . ' 09:00:00',
            'employee_id' => $this->employee->id
        ]);
        Attendance::factory()->create([
            'type' => AttendanceType::CHECK_OUT,
            'attend_at' => Carbon::today()->toDateString() . ' 18:00:00',
            'employee_id' => $this->employee->id
        ]);

        $result = $this->repository->getDetails(['employee_id' => $this->employee->id, 'date' => Carbon::tomorrow()->toDateString()]);
        $this->assertCount(0, $result);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->employee = Employee::factory()->forUser()->forDepartment()->create();
        $this->repository = new AttendanceRepository(new Attendance(), new Setting());
    }

}
