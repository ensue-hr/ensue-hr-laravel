<?php

namespace Tests\Unit\UserPreference;

use App\Modules\UserPreferences\Database\Models\UserPreference;
use App\Modules\UserPreferences\Repositories\UserPreferenceRepository;
use App\System\Employee\Database\Models\Employee;
use App\System\User\Database\Models\User;
use App\System\UserPreference\Foundation\UserPreferenceKey;
use Tests\TestCase;

class PreferenceUpdateUnitTest extends TestCase
{
    /**
     * @var \App\Modules\UserPreferences\Repositories\UserPreferenceRepository
     */
    private UserPreferenceRepository $repository;

    /**
     * @var \App\System\Employee\Database\Models\Employee
     */
    private Employee $employee;

    public function testSettingValueShouldUpdateForWorkTime(): void
    {
        $setting = $this->repository->getUserPreferencesList($this->employee->user_id, ['key' => UserPreferenceKey::EMPLOYEE_WORK_TIME])->first();

        $data = ['value' => [
            'start_work_time' => '09:00',
            'end_work_time' => '18:00',
        ]];
        $result = $this->repository->update($setting->id, $data);
        $this->assertInstanceOf(UserPreference::class, $result);
        $this->assertEquals($setting->key, $result->key);
        $this->assertEquals($data['value'], $result->value);
    }

    public function testSettingValueShouldUpdateForString(): void
    {
        $setting = $this->repository->getUserPreferencesList($this->employee->user_id, ['key' => UserPreferenceKey::USER_THEME])->first();

        $data = ['value' => 'dark'];
        $result = $this->repository->update($setting->id, $data);
        $this->assertInstanceOf(UserPreference::class, $result);
        $this->assertEquals($setting->key, $result->key);
        $this->assertEquals($data['value'], $result->value);
    }

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new UserPreferenceRepository(new UserPreference());
        $this->employee = Employee::factory()->for(User::factory()->state(['status' => User::STATUS_ACTIVE]))->forDepartment()->create();
        $this->artisan('db:seed --class=UserPreferenceSeeder');
    }
}
