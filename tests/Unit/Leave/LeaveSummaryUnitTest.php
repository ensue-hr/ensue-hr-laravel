<?php

namespace Leave;

use App\Modules\Leaves\Database\Models\AllocatedLeave;
use App\Modules\Leaves\Database\Models\Leave;
use App\Modules\Leaves\Database\Models\Setting;
use App\Modules\Leaves\Repositories\LeaveRepository;
use App\System\Employee\Database\Models\Employee;
use App\System\Leave\Foundation\LeaveStatus;
use App\System\Leave\Foundation\LeaveType;
use App\System\Setting\Foundation\SettingKey;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;

class LeaveSummaryUnitTest extends TestCase
{
    private Employee $employee;

    private Employee $manager;

    private Setting $leaveType;

    private LeaveRepository $repository;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->employee = Employee::factory()->forUser()->forDepartment()->create();
        $this->employeeAccessToken = 'Bearer ' . $this->employee->user->createToken('test12345')->accessToken;;
        $this->manager = Employee::factory()->forUser()->forDepartment()->create();
        $this->leaveType = Setting::where('key', SettingKey::LEAVES_TYPE)->first();
        $this->repository = new LeaveRepository(new Leave(), new AllocatedLeave(), new Setting(), new \App\Modules\Leaves\Database\Models\Employee());

        $this->leaveType = Setting::where('key', SettingKey::LEAVES_TYPE)->first();

        Setting::where('key', SettingKey::SYSTEM_FISCAL_DATE_START)->update(['value' => Carbon::now()->addMonths(6)->format('m-d')]);
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::now()->subMonth()->toDateString(),
            'end_date' => Carbon::now()->addMonth()->toDateString(),
            'type' => LeaveType::SICK_LEAVE,
            'days' => 15
        ]);
        Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => LeaveType::SICK_LEAVE,
            'status' => LeaveStatus::PENDING
        ]);
        Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => LeaveType::SICK_LEAVE,
            'start_at' => Carbon::now()->addDays(10)->format('Y-m-d H:i:s'),
            'end_at' => Carbon::now()->addDays(12)->subSecond()->format('Y-m-d H:i:s'),
            'status' => LeaveStatus::APPROVED
        ]);
        Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => LeaveType::UNPAID_LEAVE,
            'start_at' => Carbon::now()->addDays(14)->format('Y-m-d H:i:s'),
            'end_at' => Carbon::now()->addDays(16)->subSecond()->format('Y-m-d H:i:s'),
            'status' => LeaveStatus::CANCELLED
        ]);
    }

    public function testShouldGetSummaryOfFiscalYear(): void
    {
        $result = $this->repository->getSummary($this->employee->id);

        $leaveTypes = $this->leaveType?->value ?? [];
        $this->assertCount(count($leaveTypes) + 1, $result);
        $this->assertIsArray($result);
        $this->assertEquals($result['total']['allocated'], 15);
        $this->assertEquals($result['total']['leaves'], 2);
        $this->assertEquals($result['sick']['allocated'], 15);
        $this->assertEquals($result['sick']['leaves'], 2);
        $this->assertEquals($result['personal']['allocated'], 0);
        $this->assertEquals($result['personal']['leaves'], 0);
    }

    public function testShouldNotGetSummaryOfUnknownEmployee(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->getSummary(random_int(1000, 2000));
    }

}
