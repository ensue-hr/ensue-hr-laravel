<?php

namespace Leave;

use App\Modules\Leaves\Database\Models\AllocatedLeave;
use App\Modules\Leaves\Database\Models\Leave;
use App\Modules\Leaves\Database\Models\Setting;
use App\Modules\Leaves\Repositories\LeaveRepository;
use App\System\Employee\Database\Models\Employee;
use App\System\Employee\Database\Models\EmployeeView;
use App\System\Leave\Foundation\LeaveStatus;
use App\System\Leave\Foundation\LeaveType;
use App\System\Setting\Foundation\SettingKey;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Pagination\LengthAwarePaginator;
use Tests\TestCase;

class LeaveDetailUnitTest extends TestCase
{
    private Employee $employee;

    private Employee $manager;

    private Setting $leaveType;

    private LeaveRepository $repository;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->employee = Employee::factory()->forUser()->forDepartment()->create();
        $this->manager = Employee::factory()->forUser()->forDepartment()->create();
        $this->leaveType = Setting::where('key', SettingKey::LEAVES_TYPE)->first();

        Setting::where('key', SettingKey::SYSTEM_FISCAL_DATE_START)->update(['value' => Carbon::now()->addMonths(6)->format('m-d')]);
        AllocatedLeave::factory()->create([
            'employee_id' => $this->employee->id,
            'start_date' => Carbon::now()->subMonth()->toDateString(),
            'end_date' => Carbon::now()->addMonth()->toDateString(),
            'type' => $this->leaveType->value[0],
            'days' => 15
        ]);
        Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
            'status' => LeaveStatus::PENDING
        ]);
        Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
            'start_at' => Carbon::now()->addDays(10)->format('Y-m-d H:i:s'),
            'end_at' => Carbon::now()->addDays(12)->subSecond()->format('Y-m-d H:i:s'),
            'status' => LeaveStatus::APPROVED
        ]);
        Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => LeaveType::UNPAID_LEAVE,
            'start_at' => Carbon::now()->addDays(14)->format('Y-m-d H:i:s'),
            'end_at' => Carbon::now()->addDays(16)->subSecond()->format('Y-m-d H:i:s'),
            'status' => LeaveStatus::CANCELLED
        ]);
        $this->repository = new LeaveRepository(new Leave(), new AllocatedLeave(), new Setting(), new \App\Modules\Leaves\Database\Models\Employee());
    }

    public function testLeavesListShouldBeFetched(): void
    {
        $result = $this->repository->getList([]);
        $this->assertCount(3, $result);
        $this->assertInstanceOf(LengthAwarePaginator::class, $result);
        $this->assertInstanceOf(Leave::class, $result->first());
    }

    public function testLeaveListIsFilteredByStatus(): void
    {
        Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
            'start_at' => Carbon::now()->addDays(15)->format('Y-m-d H:i:s'),
            'end_at' => Carbon::now()->addDays(17)->subSecond()->format('Y-m-d H:i:s'),
            'status' => LeaveStatus::APPROVED
        ]);
        $result = $this->repository->getList(['status' => LeaveStatus::APPROVED, 'keyword' => '']);

        $this->assertCount(2, $result);
    }

    public function testLeaveListIsFilteredByTitle(): void
    {
        $leave = Leave::first();
        $result = $this->repository->getList(['title' => $leave->title]);

        $this->assertCount(1, $result);
    }

    public function testLeaveListIsFilteredByType(): void
    {
        $result = $this->repository->getList(['type' => LeaveType::UNPAID_LEAVE]);

        $this->assertCount(1, $result);
    }

    public function testLeaveListIsFilteredByDateRange(): void
    {
        $result = $this->repository->getList([
            'start_at' => Carbon::today()->addDays(7)->format('Y-m-d H:i:s'),
            'end_at' => Carbon::today()->addDays(19)->subSecond()->format('Y-m-d H:i:s'),
        ]);

        $this->assertCount(3, $result);
    }

    public function testLeaveListIsFilteredByEmployeeIdAndRequestedTo(): void
    {
        $result = $this->repository->getList(['employee_id' => $this->employee->id, 'requested_to' => $this->manager->id]);

        $this->assertCount(3, $result);
    }

    public function testLeaveListEmptyWithWrongParameters(): void
    {
        $result = $this->repository->getList(['status' => 'xyz', 'keyword' => 'xyz', 'employee_id' => 123, 'requested_to' => 123]);

        $this->assertCount(0, $result);
    }

    public function testEmployeeDetailShouldFetch(): void
    {
        $leave = Leave::factory()->create([
            'requested_to' => $this->manager->id,
            'employee_id' => $this->employee->id,
            'type' => $this->leaveType->value[0],
            'start_at' => Carbon::now()->addDays(15)->format('Y-m-d H:i:s'),
            'end_at' => Carbon::now()->addDays(17)->subSecond()->format('Y-m-d H:i:s'),
            'status' => LeaveStatus::APPROVED
        ]);
        $result = $this->repository->getById($leave->id);
        $this->assertInstanceOf(Leave::class, $result);
        $this->assertEquals($leave->id, $result->id);
        $this->assertEquals($leave->title, $result->title);
        $this->assertEquals($leave->start_at, $result->start_at);
        $this->assertEquals($leave->end_at, $result->end_at);
        $this->assertEquals($leave->days, $result->days);
        $this->assertEquals($leave->type, $result->type);
        $this->assertEquals($leave->status, $result->status);
        $this->assertEquals($leave->is_emergency, $result->is_emergency);
        $this->assertInstanceOf(EmployeeView::class, $result->requested);
        $this->assertEquals($this->manager->id, $result->requested->id);
        $this->assertInstanceOf(EmployeeView::class, $result->employee);
        $this->assertEquals($this->employee->id, $result->employee->id);
    }

    public function testLeaveDetailShouldNotFetchForUnknownLeave(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->getById(random_int(1000, 2000));
    }

}
