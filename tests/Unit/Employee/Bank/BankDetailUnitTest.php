<?php


namespace Employee\Bank;


use App\Modules\Employees\Database\Models\Employee;
use App\Modules\Employees\Repositories\BankRepository;
use App\System\Common\Database\Models\Bank;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use NicoSystem\Foundation\Status;
use Tests\TestCase;

class BankDetailUnitTest extends TestCase
{
    /**
     * @var \App\Modules\Employees\Repositories\BankRepository
     */
    private BankRepository $repository;

    /**
     * @var \App\System\Employee\Database\Models\Employee
     */
    private \App\System\Employee\Database\Models\Employee $employee;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new BankRepository(new Employee(), new \App\Modules\Employees\Database\Models\Bank());
        $this->employee = Employee::factory()->forUser()->forDepartment()->create();
    }

    public function testBankDetailShouldFetch(): void
    {
        $bank = $this->employee->bank()->create(Bank::factory()->raw());
        $result = $this->repository->getBankById($this->employee->id, $bank->id);
        $this->assertEquals($result->id, $bank->id);
        $this->assertEquals($result->name, $bank->name);
        $this->assertEquals($result->branch, $bank->branch);
        $this->assertEquals($result->account_number, $bank->account_number);
        $this->assertEquals($result->status, $bank->status);
    }

    public function testBankDetailShouldNotFetchForUnknownEmployee(): void
    {
        $bank = $this->employee->bank()->create(Bank::factory()->raw());
        $this->expectException(ModelNotFoundException::class);
        $this->repository->getBankById(random_int(10000, 20000), $bank->id);
    }

    public function testBankDetailShouldNotFetchForUnknownBank(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->getBankById($this->employee->id, random_int(10000, 20000));
    }

    public function testBankListShouldBeFetched(): void
    {
        $this->employee->bank()->create(Bank::factory()->raw());
        $result = $this->repository->getBankList(['keyword' => ''], $this->employee->id);
        $this->assertCount(1, $result);
    }

    public function testBankListShouldNotBeFetchedForUnknownEmployee(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->getBankList([], random_int(1000, 2000));
    }

    public function testBankListAreSearchable(): void
    {
        $bank = $this->employee->bank()->create(Bank::factory()->raw());
        $result = $this->repository->getBankList(['keyword' => $bank->name], $this->employee->id);
        $this->assertEquals($bank->name, $result->first()->name);
    }

    public function testBankListIsFilteredByStatus(): void
    {
        $this->employee->bank()->create(Bank::factory()->raw());
        $this->employee->bank()->create(Bank::factory()->raw(['status' => Status::STATUS_UNPUBLISHED]));
        $result = $this->repository->getBankList(['status' => Status::STATUS_PUBLISHED, 'keyword' => ''], $this->employee->id);

        $this->assertCount(1, $result);
    }

    public function testBankListEmptyWithWrongParameters(): void
    {
        $this->employee->bank()->create(Bank::factory()->raw());
        $result = $this->repository->getBankList(['status' => 'xyz', 'keyword' => 'xyz'], $this->employee->id);

        $this->assertCount(0, $result);
    }
}
