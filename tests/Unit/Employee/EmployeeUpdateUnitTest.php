<?php


namespace Employee;


use App\Modules\Employees\Database\Models\Employee;
use App\Modules\Employees\Database\Models\Setting;
use App\Modules\Employees\Database\Models\User;
use App\Modules\Employees\Repositories\EmployeeRepository;
use App\System\Employee\Database\Models\Employee as SystemEmployee;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;

class EmployeeUpdateUnitTest extends TestCase
{
    /**
     * @var EmployeeRepository
     */
    private EmployeeRepository $repository;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new EmployeeRepository(new Employee(), new User(), new Setting());
    }

    public function testBasicEmployeeUpdate(): void
    {
        $existedEmployee = SystemEmployee::factory()->forUser()->forDepartment()->create();
        $data = User::factory()->raw([
            'email' => $existedEmployee->user->email,
        ]);
        $data += Employee::factory()->raw([
            'code' => $existedEmployee->code,
            'department_id' => $existedEmployee->department->id
        ]);
        $this->repository->update($existedEmployee->id, $data);
        $this->assertDatabaseHas('users', [
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'middle_name' => $data['middle_name'],
            'email' => $data['email'],
        ]);
        $this->assertDatabaseHas('employees', [
            'code' => $data['code'],
            'personal_email' => $data['personal_email'],
            'position' => $data['position'],
            'detached_at' => null,
            'gender' => $data['gender'],
            'dob' => $data['dob'],
            'marital_status' => $data['marital_status'],
            'pan_no' => $data['pan_no'],
            'department_id' => $data['department_id']
        ]);
    }

    /**
     * @throws \Exception
     */
    public function testEmployeeShouldNotUpdateForUnknownEmployee(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->update(random_int(1000, 2000), []);
    }

}
