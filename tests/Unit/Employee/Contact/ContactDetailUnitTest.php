<?php


namespace Employee\Contact;


use App\Modules\Employees\Database\Models\Employee;
use App\Modules\Employees\Repositories\ContactRepository;
use App\Modules\Employees\Database\Models\Contact;
use App\System\Common\Foundation\ContactType;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use NicoSystem\Foundation\Status;
use Tests\TestCase;

class ContactDetailUnitTest extends TestCase
{

    /**
     * @var \App\System\Employee\Database\Models\Employee
     */
    private \App\System\Employee\Database\Models\Employee $employee;

    /**
     * @var \App\Modules\Employees\Repositories\ContactRepository
     */
    private ContactRepository $repository;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new ContactRepository(new Employee(), new Contact());
        $this->employee = Employee::factory()->forUser()->forDepartment()->create();
    }


    public function testContactDetailShouldFetch(): void
    {
        $contact = Contact::factory()->create(['contactable_id' => $this->employee->id, 'contactable_type' => 'Employee']);
        $result = $this->repository->getContactById($this->employee->id, $contact->id);
        $this->assertInstanceOf(\App\System\Common\Database\Models\Contact::class, $result);
        $this->assertEquals($contact->id, $result->id);
        $this->assertEquals($contact->type, $result->type);
        $this->assertEquals($contact->status, $result->status);
    }

    public function testContactDetailShouldNotFetchForUnknownEmployee(): void
    {
        $contact = Contact::factory()->create(['contactable_id' => $this->employee->id, 'contactable_type' => 'Employee']);
        $this->expectException(ModelNotFoundException::class);
        $this->repository->getContactById(random_int(10000, 20000), $contact->id);
    }

    public function testContactDetailShouldNotFetchForUnknownContact(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->getContactById($this->employee->id, random_int(10000, 20000));
    }

    public function testContactListShouldBeFetched(): void
    {
        Contact::factory()->count(3)->create(['contactable_id' => $this->employee->id, 'contactable_type' => 'Employee']);
        $result = $this->repository->getContactList([], $this->employee->id);
        $this->assertCount(3, $result);
    }

    public function testContactListShouldNotBeFetchedForUnknownEmployee(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->getContactList([], random_int(1000, 2000));
    }

    public function testContactListAreSortedAndOrderedAndSearchable(): void
    {
        $contacts = Contact::factory()->count(2)->create(['contactable_id' => $this->employee->id, 'contactable_type' => 'Employee']);
        $contacts = $contacts->sortByDesc('number')->pluck('number');
        $result = $this->repository->getContactList(['sort_by' => 'number', 'sort_order' => 'desc'], $this->employee->id);
        $result = $result->getCollection()->pluck('number');
        $this->assertEquals($contacts, $result);

        $contact = Contact::factory()->create(['contactable_id' => $this->employee->id, 'contactable_type' => 'Employee']);
        $result = $this->repository->getContactList(['keyword' => $contact->number], $this->employee->id);
        $this->assertEquals($contact->number, $result->first()->number);
    }

    public function testContactListIsFilteredByType(): void
    {
        Contact::factory()->count(1)->create(['contactable_id' => $this->employee->id, 'contactable_type' => 'Employee', 'type' => ContactType::TYPE_PERSONAL]);
        Contact::factory()->count(2)->create(['contactable_id' => $this->employee->id, 'contactable_type' => 'Employee', 'type' => ContactType::TYPE_HOME]);
        Contact::factory()->count(3)->create(['contactable_id' => $this->employee->id, 'contactable_type' => 'Employee', 'type' => ContactType::TYPE_WORK]);
        $result = $this->repository->getContactList(['type' => ContactType::TYPE_WORK], $this->employee->id);

        $this->assertCount(3, $result);
    }

    public function testContactListIsFilteredByStatus(): void
    {
        Contact::factory()->count(1)->create(['contactable_id' => $this->employee->id, 'contactable_type' => 'Employee']);
        Contact::factory()->count(1)->create(['contactable_id' => $this->employee->id, 'contactable_type' => 'Employee', 'status' => Status::STATUS_UNPUBLISHED]);
        $result = $this->repository->getContactList(['status' => Status::STATUS_PUBLISHED, 'keyword' => ''], $this->employee->id);

        $this->assertCount(1, $result);
    }

    public function testContactListEmptyWithWrongParameters(): void
    {
        Contact::factory()->count(1)->create(['contactable_id' => $this->employee->id, 'contactable_type' => 'Employee']);
        Contact::factory()->count(1)->create(['contactable_id' => $this->employee->id, 'contactable_type' => 'Employee', 'status' => Status::STATUS_UNPUBLISHED]);
        $result = $this->repository->getContactList(['status' => 'xyz', 'keyword' => 'xyz'], $this->employee->id);

        $this->assertCount(0, $result);
    }
}
