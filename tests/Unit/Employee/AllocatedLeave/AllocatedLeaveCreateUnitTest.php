<?php


namespace Employee\AllocatedLeave;


use App\Modules\Employees\Database\Models\Employee;
use App\Modules\Employees\Database\Models\AllocatedLeave;
use App\Modules\Employees\Database\Models\Setting;
use App\Modules\Employees\Repositories\AllocatedLeaveRepository;
use App\System\Setting\Database\Traits\FiscalYearTrait;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;

class AllocatedLeaveCreateUnitTest extends TestCase
{
    use FiscalYearTrait;

    /**
     * @var \App\Modules\Employees\Repositories\AllocatedLeaveRepository
     */
    private AllocatedLeaveRepository $repository;

    /**
     * @var \App\System\Employee\Database\Models\Employee
     */
    private \App\System\Employee\Database\Models\Employee $employee;

    /**
     * @var \App\Modules\Employees\Database\Models\Setting
     */
    private Setting $setting;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->setting = new Setting();
        $this->repository = new AllocatedLeaveRepository(new Employee(), new AllocatedLeave(), new Setting());
        $this->employee = Employee::factory()->forUser()->forDepartment()->create();
    }

    public function testAllocateLeave(): void
    {
        $data = AllocatedLeave::factory()->raw(['current_fiscal_year' => true]);
        $result = $this->repository->createLeave($data, $this->employee->id);
        $this->assertInstanceOf(AllocatedLeave::class, $result);
        $data['employee_id'] = $this->employee->id;
        $fiscalDates = $this->getStartAndEndFiscalDate();
        $data = array_merge($data, $fiscalDates);
        unset($data['current_fiscal_year']);
        $this->assertDatabaseHas('allocated_leaves', $data);
    }

    public function testAllocateLeaveForNextFiscalYear(): void
    {
        $data = AllocatedLeave::factory()->raw(['current_fiscal_year' => false]);
        $result = $this->repository->createLeave($data, $this->employee->id);
        $this->assertInstanceOf(AllocatedLeave::class, $result);
        $data['employee_id'] = $this->employee->id;
        $fiscalDates = $this->getStartAndEndFiscalDate(false);
        $data = array_merge($data, $fiscalDates);
        unset($data['current_fiscal_year']);
        $this->assertDatabaseHas('allocated_leaves', $data);
    }

    /**
     * @throws \Exception
     */
    public function testLeaveShouldNotBeAllocatedForUnknownEmployee(): void
    {
        $data = AllocatedLeave::factory()->raw(['current_fiscal_year' => true]);
        $this->expectException(ModelNotFoundException::class);
        $this->repository->createLeave($data, random_int(1000, 2000));
    }
}
