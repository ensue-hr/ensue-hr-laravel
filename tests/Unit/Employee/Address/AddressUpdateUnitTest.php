<?php


namespace Employee\Address;


use App\Modules\Employees\Database\Models\Address;
use App\Modules\Employees\Database\Models\Employee;
use App\Modules\Employees\Repositories\AddressRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;

class AddressUpdateUnitTest extends TestCase
{

    /**
     * @var \App\System\Employee\Database\Models\Employee
     */
    private \App\System\Employee\Database\Models\Employee $employee;

    /**
     * @var \App\Modules\Employees\Repositories\AddressRepository
     */
    private AddressRepository $repository;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new AddressRepository(new Employee(), new Address());
        $this->employee = Employee::factory()->forUser()->forDepartment()->create();
    }


    public function testAddressShouldUpdate(): void
    {
        $address = Address::factory()->create(['addressable_id' => $this->employee->id, 'addressable_type' => 'Employee']);
        $data = Address::factory()->raw();
        unset($data['type']);
        $result = $this->repository->updateAddress($data, $this->employee->id, $address->id);
        $this->assertInstanceOf(Address::class, $result);
        $data['addressable_id'] = $this->employee->id;
        $data['addressable_type'] = 'Employee';
        $this->assertDatabaseHas('addresses', $data);
    }

    public function testAddressShouldNotUpdateForUnknownEmployee(): void
    {
        $address = Address::factory()->create(['addressable_id' => $this->employee->id, 'addressable_type' => 'Employee']);
        $this->expectException(ModelNotFoundException::class);
        $this->repository->updateAddress([], random_int(10000, 20000), $address->id);
    }

    public function testAddressShouldNotUpdateForUnknownAddress(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->updateAddress([], $this->employee->id, random_int(10000, 20000));
    }
}
