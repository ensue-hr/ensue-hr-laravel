<?php

namespace ActivityLog;

use App\Modules\ActivityLogs\Database\Models\ActivityLog;
use App\Modules\ActivityLogs\Repositories\ActivityLogRepository;
use App\System\ActivityLog\ActivityLogger;
use App\System\Department\Database\Models\Department;
use App\System\Employee\Database\Models\Employee;
use App\System\User\Database\Models\User;
use Illuminate\Pagination\LengthAwarePaginator;
use Tests\TestCase;

class ActivityLogUnitTest extends TestCase
{
    /**
     * @var \App\Modules\ActivityLogs\Repositories\ActivityLogRepository
     */
    private ActivityLogRepository $repository;

    /**
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new ActivityLogRepository(new ActivityLog());
    }

    public function testActivityLogShouldBeList(): void
    {
        $employee = Employee::factory()->forUser()->forDepartment()->create();
        $user = $employee->user;

        $logger = new ActivityLogger();
        $logger->setTitle('users.login')->saveLog($user->id);
        $logger->setTitle('users.sso.google')->saveLog($user->id);
        $logger->setTitle('users.sso.microsoft')->saveLog($user->id);

        $result = $this->repository->getList();
        $this->assertCount(3, $result);
        $this->assertInstanceOf(LengthAwarePaginator::class, $result);
    }

    public function testActivityLogDetail(): void
    {
        $employee = Employee::factory()->forUser()->forDepartment()->create();
        $user = $employee->user;

        $logger = new ActivityLogger();
        $logger->setTitle('users.login')->saveLog($user->id);

        $log = ActivityLog::first();
        $result = $this->repository->getById($log->id);
        $this->assertInstanceOf(ActivityLog::class, $result);
        $this->assertInstanceOf(User::class, $result->user);
    }

    public function testWhenDepartmentIsCreatedActivityLogShouldBeAdded(): void
    {
        Department::setActivityRecord(true)->factory()->create();

        $result = $this->repository->getList();
        $this->assertCount(1, $result);
    }
}
