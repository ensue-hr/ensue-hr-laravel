<?php


namespace Authentication;


use App\System\Employee\Database\Models\Employee;
use App\System\User\Database\Models\User;
use NicoSystem\Foundation\Status;
use Tests\TestCase;

class AuthUnitTest extends TestCase
{

    public function testOwnUserDetailShouldFetch(): void
    {
        $user = User::factory()->create(['status' => Status::STATUS_PUBLISHED]);
        $headers = ['Authorization' => 'Bearer ' . $user->createToken('test')->accessToken];
        $result = $this->get('api/auth/me', $headers);
        $result->assertJson($user->toArray());
    }

    public function testOwnDetailShouldBeFetchedIfIAmEmployee(): void
    {
        $employee = Employee::factory()->forUser()->forDepartment()->hasAddresses(2)->hasContacts(2)->hasBank()->create();
        $user = $employee->user;
        $headers = ['Authorization' => 'Bearer ' . $user->createToken('test')->accessToken];
        $result = $this->get('api/auth/me/details', $headers);

        $expectedJson = [
            'id' => $employee->id,
            'email' => $employee->user->email,
            'status' => $employee->user->status,
            'avatar' => $employee->user->avatar,
            'code' => $employee->code,
            'personal_email' => $employee->personal_email,
            'position' => $employee->position,
            'joined_at' => $employee->joined_at,
            'detached_at' => $employee->detached_at,
            'gender' => $employee->gender,
            'dob' => $employee->dob,
            'pan_no' => $employee->pan_no,
            'department' => $employee->department->title
        ];
        $expectedJson['addresses'] = $employee->addresses->toArray();
        $expectedJson['contacts'] = $employee->contacts->toArray();
        $expectedJson['bank'] = $employee->bank->toArray();

        $result->assertJson($expectedJson);
    }

    public function testOwnDetailEmptyIfNotEmployee(): void
    {
        $user = User::factory()->create(['status' => Status::STATUS_PUBLISHED]);
        $headers = ['Authorization' => 'Bearer ' . $user->createToken('test')->accessToken];
        $result = $this->get('api/auth/me/details', $headers);
        $this->assertEquals("{}", $result->getContent());
    }

    public function testValidationErrorShouldBeThrownForInvalidAvatarUrl(): void
    {
        $user = User::factory()->create(['status' => Status::STATUS_PUBLISHED]);
        $headers = ['Authorization' => 'Bearer ' . $user->createToken('test')->accessToken];
        $result = $this->put('api/auth/me/avatar', ['avatar' => $this->faker->url], $headers);
        $result->assertStatus(417);
        $result->assertJson([
            'avatar' => ['The avatar must end with one of the following: .jpg, .png, .jpeg.']
        ]);
    }

    public function testOwnAvatarShouldBeUpdated(): void
    {
        $user = User::factory()->create(['status' => Status::STATUS_PUBLISHED]);
        $headers = ['Authorization' => 'Bearer ' . $user->createToken('test')->accessToken];
        $data = ['avatar' => 'http://localhost:8000/test.png'];
        $result = $this->put('api/auth/me/avatar', $data, $headers);
        $result->assertStatus(200);
        $data['id'] = $user->id;
        $result->assertJson($data, $result->getContent());
        $this->assertDatabaseHas('users', $data);
    }

    public function testOwnAvatarShouldBeRemoved(): void
    {
        $user = User::factory()->create(['status' => Status::STATUS_PUBLISHED]);
        $headers = ['Authorization' => 'Bearer ' . $user->createToken('test')->accessToken];
        $result = $this->put('api/auth/me/avatar', [], $headers);
        $result->assertStatus(200);
        $data = [
            'id' => $user->id,
            'avatar' => null
        ];
        $result->assertJson($data, $result->getContent());
        $this->assertDatabaseHas('users', $data);
    }
}
