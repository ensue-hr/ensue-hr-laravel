<?php

namespace Tests;

use App\System\AppBaseModel;
use App\System\User\Database\Models\User;
use Faker\Factory as Faker;
use Faker\Generator;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, DatabaseMigrations;

    /**
     * @var \Faker\Generator
     */
    protected Generator $faker;

    /**
     * Setup unit test
     * @setup
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->artisan('passport:install');
        $this->faker = Faker::create();
        $user = User::setActivityRecord(false)->factory()->create();
        $this->be($user);
        $this->seed();
    }

    /**
     * End unit test
     */
    protected function tearDown(): void
    {
        $this->artisan('migrate:refresh');
        \Mockery::close();
        parent::tearDown();
    }

    /**
     *Make the validator
     *
     * @param $request
     * @param $field
     * @param $value
     * @return mixed
     */
    protected function getFieldValidator($request, $field, $value): mixed
    {
        $rules = $request->rules();
        return $this->app['validator']->make([$field => $value], [$field => $rules[$field]]);
    }

    /**
     *
     * make a validator for testing validation if it passes or not
     *
     * @param $request
     * @param $field
     * @param $value
     * @return mixed
     */
    protected function validateField($request, $field, $value): mixed
    {
        return $this->getFieldValidator($request, $field, $value)->passes();
    }
}
