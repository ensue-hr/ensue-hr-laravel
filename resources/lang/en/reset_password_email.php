<?php
return [
    "subject"=>config("app.name") . ": Password reset link",
    "title"=>"Password reset link",
    "body"=>"<p>You are receiving this email because we received a password reset request for your account.</p>
                <p><a href=':url' title='password reset link' class='btn btn-primary'>Reset Password</a></p>
            <p>
            If you didn't initiate this request password, please forward it to ENSUE HR Support Personnel or your primary contact.  Do not simply ignore this email.
            </p>
            <p><small><em>This link will expire in ". config('mail.expiration',120)/60 ." hours</em></small></p>
            ",
];
