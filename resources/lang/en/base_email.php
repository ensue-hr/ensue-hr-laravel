<?php
return [
    "title" => config("app.name") . ": :title",
    "salutation" => "<p> Dear :receiverName,</p>",
    "regards"=>"<p>With Regards,<br>ENSUE Nepal <br>Management Team</p>",
];
