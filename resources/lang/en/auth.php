<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'invalid_provider' => 'Please provide correct provider to login.',
    'reset_link' => 'Reset link set to the email',
    'old_password'=>'Old password must be valid',
    'recent_password'=>'Your new password can\'t be same as your recent password',
    'password_success' => 'Password changed successfully',
    'unauthorized' => 'The user credentials were incorrect.',
    'multiple_attempts' => 'User account is disable due to too many wrong attempt',
    'forbidden' => 'You are not authorized to perform this action.',
];
